## Status
       
[![Latest Stable Version](https://poser.pugx.org/fbkl/adonis/v/stable)](https://packagist.org/packages/fbkl/adonis)
[![pipeline status](https://gitlab.com/fabrika-klientov/libraries/adonis/badges/master/pipeline.svg)](https://gitlab.com/fabrika-klientov/libraries/adonis/commits/master)
[![License](https://poser.pugx.org/fbkl/adonis/license)](https://packagist.org/packages/fbkl/adonis)




**Клиент для работы с API Укр почты (УП)**

[Документация](https://dev.ukrposhta.ua/documentation)

---

## Usage

`composer require fbkl/adonis`

---

## Работа с библиотекой

### Client (Клиент для работы с УП)

**Некоторые методы закрыты контрактом учетной записи в УП.**

Для начала работы с библиотекой УП необходимо создать инстанс клиента, передав ему 
ваши токены получены в процессе заключения договора с УП

1. Создайте объект `Credentials`:
```injectablephp
use Adonis\Core\Auth\Credentials;

// Все параметры опциональны
$credentials = new Credentials(
    'xxxxxxxx-yyyy-zzzz-cccc-111111111111', // PRODUCTION BEARER eCom
    'xxxxxxxx-yyyy-zzzz-cccc-222222222222', // PRODUCTION BEARER StatusTracking
    'xxxxxxxx-yyyy-zzzz-cccc-333333333333', // PROD_COUNTERPARTY TOKEN
    'xxxxxxxx-yyyy-zzzz-cccc-444444444444', // SANDBOX BEARER eCom
    'xxxxxxxx-yyyy-zzzz-cccc-555555555555', // SANDBOX BEARER StatusTracking
    'xxxxxxxx-yyyy-zzzz-cccc-666666666666', // SAND_COUNTERPARTY TOKEN
    'xxxxxxxx-yyyy-zzzz-cccc-777777777777' // COUNTERPARTY UUID
);

// или
$credentials = new Credentials();
$credentials->setEComToken('xxxxxxxx-yyyy-zzzz-cccc-111111111111');
$credentials->setStatusTrackingToken('xxxxxxxx-yyyy-zzzz-cccc-222222222222');
$credentials->setCounterpartyToken('xxxxxxxx-yyyy-zzzz-cccc-333333333333');
$credentials->setEComTokenSand('xxxxxxxx-yyyy-zzzz-cccc-444444444444');
$credentials->setStatusTrackingTokenSand('xxxxxxxx-yyyy-zzzz-cccc-555555555555');
$credentials->setCounterpartyTokenSand('xxxxxxxx-yyyy-zzzz-cccc-666666666666');
$credentials->setCounterpartyUuid('xxxxxxxx-yyyy-zzzz-cccc-777777777777');

```
Необязательно добавлять все ключи в `Credentials`
(добавляйте по мере использования клиента)

2. Создайте клиент `Client`
(если передать второй параметр `true` - Клиент будет использоваться
в тест режиме, для этого необходимы тест токены авторизации):
```injectablephp
use Adonis\Client;

$client = new Client($credentials);
$clientTest = new Client($credentials, true);

```

3. С помощью клиента вы можете создать (вызвать) нужный сервис для работы:
```injectablephp
$service1 = $client->getAddressClassifierService();
$service2 = $client->getShipmentsService();
$service3 = $client->getFormsService();
// ...
```

### Services

Доступные сервисы:
- `AddressClassifierService` - Поиск индексов и отделений
- `AddressesService` - Работа с адресами
- `ClientsService` - Работа с клиентами 
- `ShipmentsService` - Работа с почтовыми отправлениями
- `DeliveryNotificationsService` - Работа с сообщениями о вручении 
- `DocumentBackService` - Работа с обратной доставкой документов
- `FormsService` - Работа с печатными формами
- `LettersService` - Работа с письмами _(Необходима доработка)_ 
- `PaginatorService` - Работа с пагинацией 
- `ShipmentGroupsService` - Работа с группами отправлений
- `StatusTrackingService` - Работа с статус трекингом 
- `TariffsService` - Работа с тарифами

---

Во время работы с сервисами - все запросы строятся с помощью
объекта конструктора запросов на вход необходимого метода сервиса
(если эти данные нужны методу).

Результат выполнения будет всегда объект (сущность) определенного класса
или коллекция этих объектов (сущностей), за исключением `FormsService` -
в результате бинарные данные формы (строка).

`TODO: По надобности расписать работу по каждому сервису и его методах отдельно`

### Service AddressClassifierService

...

### Service AddressesService

...

---

### Create Shipments

```injectablephp
// Получает сервис работы с адресами
$addressesService = $client->getAddressesService();

$requestAddress1 = new \Adonis\Entities\Addresses\Requests\StoreAddressRequest();
$request->postcode('01001');
// Создаем первый адрес (с минимальным набором данных)
$address1 = $addressesService->store($requestAddress1);

$requestAddress2 = new \Adonis\Entities\Addresses\Requests\StoreAddressRequest();
$request
    ->postcode('07401')
    ->region('Київська')
    ->city('Бровари')
    ->district('Київський')
    ->street('Котляревського')
    ->houseNumber(12)
    ->apartmentNumber(33);
// Создаем второй адрес
$address2 = $addressesService->store($requestAddress2);


// Получает сервис работы с клиентами
$clientsService = $client->getClientsService();

$requestClient1 = new \Adonis\Entities\Clients\Requests\StoreClientRequest();
$requestClient1
    ->type(\Adonis\Entities\Clients\Requests\StoreClientRequest::INDIVIDUAL)
    ->firstName('Король')
    ->lastName('Ричард')
    ->addressId($address1->id);
// Создаем первый клиент
$client1 = $clientsService->store($requestClient1);

$requestClient2 = new \Adonis\Entities\Clients\Requests\StoreClientRequest();
$requestAddress2 = new \Adonis\Entities\Clients\Requests\ClientAddressRequest();
$requestAddress2->addressId($address1->id);
$requestClient2
    ->type(\Adonis\Entities\Clients\Requests\StoreClientRequest::COMPANY)
    ->name('ФОП Осликов')
    ->addresses([$requestAddress2])
    ->addressId($address2->id)
    ->phoneNumber('0688888888')
    ->edrpou('14360570')
    ->bankAccount('UA173000010000032001234567890');
// Создаем второй клиент
$client2 = $clientsService->store($requestClient2);


// Получает сервис работы с отправлениями
$shipmentsService = $client->getShipmentsService();

$requestShipment = new \Adonis\Entities\Shipments\Requests\StoreShipmentRequest();

$requestSender = new \Adonis\Entities\Shipments\Requests\StoreShipmentClientRequest();
$requestSender->uuid($client1->uuid);

$requestRecipient = new \Adonis\Entities\Shipments\Requests\StoreShipmentClientRequest();
$requestRecipient->uuid($client2->uuid);

$parcel1 = new \Adonis\Entities\Shipments\Requests\StoreShipmentParcelRequest();
$parcel1
    ->weight(9)
    ->length(20)
    ->declaredPrice(400)
    ->description('desc 1');
$parcel2 = clone $parcel1;
$parcel2
    ->description('desc 2');
    
$requestShipment
    ->sender($requestSender)
    ->recipient($requestRecipient)
    ->type(\Adonis\Entities\Shipments\Requests\StoreShipmentRequest::TYPE_EXPRESS)
    ->deliveryType(\Adonis\Entities\Shipments\Requests\StoreShipmentRequest::DELIVERY_TYPE_W2W)
    ->parcels([
        $parcel1,
        $parcel2,
    ])
    ->recipientPhone('0977777777');

// Создаем отправление
$shipment = $shipmentsService->store($requestShipment);


// Создадим группу отправлений и добавим туда созданное отправление

// Получает сервис работы с группами отправлений
$shipmentGroupsService = $client->getShipmentGroupsService();

$requestShipmentGroup = new \Adonis\Entities\ShipmentGroups\Requests\StoreShipmentGroupRequest();
$requestShipmentGroup
    ->name('group name')
    ->clientUuid($client1->uuid);

// Создаем группу
$shipmentGroup = $shipmentGroupsService->store($requestShipmentGroup);

// Добавляем отправление у только что созданную группу
$message = $shipmentGroupsService->addShipmentToGroup($shipmentGroup->uuid, $shipment->uuid);

```

Есть методы массового создания отправлений или групп,
или создание отправлений в уже созданной группе отправлений
и много других методов которые ускорят массовое создание отправлений.
См выше каждый сервис по отдельности (пока не реализовано), или просмотром кода
нужного сервиса (Сервисы упрощены для удобства их чтения,
методы просты и понятны и типизированы).

