<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Adonis
 * @category  Entries
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Adonis\Entities\AddressClassifier;

use Adonis\Contracts\BeEntity;

/**
 * @property-read string $LOCK_REASON
 * @property-read string $DAYOFWEEK_UA
 * @property-read string $POSTOFFICE_TYPE
 * @property-read string $INTERVALTYPE
 * @property-read string $LOCK_CODE
 * @property-read string $SHORTNAME
 * @property-read string $POSTCODE
 * @property-read string $DAYOFWEEK_EN
 * @property-read string $POSTOFFICE_PARENT
 * @property-read string $DAYOFWEEK_SHORTNAME_UA
 * @property-read string $TTO
 * @property-read string $CITY_ID
 * @property-read string $CITY
 * @property-read string $WORKCOMMENT
 * @property-read string $id
 * @property-read string $FULLNAME
 * @property-read string $DAYOFWEEK_RU
 * @property-read string $ISVPZ
 * @property-read string $TFROM
 * */
class PostOfficeMobileOpenHours extends Entity implements BeEntity
{

}
