<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Adonis
 * @category  Entries
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Adonis\Entities\AddressClassifier\Query;

use Adonis\Contracts\BeRequestEntity;

class PostOfficesPostCodeQuery extends BaseQuery implements BeRequestEntity
{
    public function setCityKOATUU($value)
    {
        return $this->with('city_koatuu', $value);
    }

    public function setCityKATOTTG($value)
    {
        return $this->with('city_katottg', $value);
    }

    public function setCityVPZ_KATOTTG($value)
    {
        return $this->with('city_vpz_katottg', $value);
    }
}
