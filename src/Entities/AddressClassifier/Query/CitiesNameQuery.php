<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Adonis
 * @category  Entries
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Adonis\Entities\AddressClassifier\Query;

use Adonis\Contracts\BeRequestEntity;

class CitiesNameQuery extends BaseQuery implements BeRequestEntity
{
    public function setRegionId($value)
    {
        return $this->with('region_id', $value);
    }

    public function setDistrictId($value)
    {
        return $this->with('district_id', $value);
    }

    public function setCityName(string $value)
    {
        return $this->with('city_name', $value);
    }

    public function setLang(string $value)
    {
        return $this->with('lang', $value);
    }

    public function setFuzzy(bool $value)
    {
        return $this->with('fuzzy', $value ? 1 : 0);
    }
}
