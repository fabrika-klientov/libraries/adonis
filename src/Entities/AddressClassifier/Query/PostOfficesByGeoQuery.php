<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Adonis
 * @category  Entries
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Adonis\Entities\AddressClassifier\Query;

use Adonis\Contracts\BeRequestEntity;

class PostOfficesByGeoQuery extends BaseQuery implements BeRequestEntity
{
    public function setLatitude($value)
    {
        return $this->with('lat', $value);
    }

    public function setLongitude($value)
    {
        return $this->with('long', $value);
    }

    public function setMaxDistance($value)
    {
        return $this->with('maxdistance', $value);
    }
}
