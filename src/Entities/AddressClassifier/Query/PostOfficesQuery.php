<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Adonis
 * @category  Entries
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Adonis\Entities\AddressClassifier\Query;

use Adonis\Contracts\BeRequestEntity;

class PostOfficesQuery extends BaseQuery implements BeRequestEntity
{
    public function setPostIndex($value)
    {
        return $this->with('pi', $value);
    }

    public function setPostCode($value)
    {
        return $this->with('pc', $value);
    }

    public function setCityId($value)
    {
        return $this->with('poCityId', $value);
    }

    public function setDistrictId($value)
    {
        return $this->with('poDistrictId', $value);
    }

    public function setStreetId($value)
    {
        return $this->with('poStreetId', $value);
    }

    public function setRegionId($value)
    {
        return $this->with('poRegionId', $value);
    }

    public function setDCityId($value)
    {
        return $this->with('pdCityId', $value);
    }

    public function setDDistrictId($value)
    {
        return $this->with('pdDistrictId', $value);
    }

    public function setDRegionId($value)
    {
        return $this->with('pdRegionId', $value);
    }
}
