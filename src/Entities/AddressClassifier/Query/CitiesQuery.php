<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Adonis
 * @category  Entries
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Adonis\Entities\AddressClassifier\Query;

use Adonis\Contracts\BeRequestEntity;

class CitiesQuery extends BaseQuery implements BeRequestEntity
{
    public function setDistrictId($value)
    {
        return $this->with('district_id', $value);
    }

    public function setRegionId($value)
    {
        return $this->with('region_id', $value);
    }

    public function setCity(string $name)
    {
        return $this->with('city_ua', $name);
    }

    public function setKOATUU($value)
    {
        return $this->with('koatuu', $value);
    }

    public function setKATOTTG($value)
    {
        return $this->with('katottg', $value);
    }
}
