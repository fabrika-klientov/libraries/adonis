<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Adonis
 * @category  Entries
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Adonis\Entities\AddressClassifier\Query;

use Adonis\Contracts\BeRequestEntity;

class StreetsQuery extends BaseQuery implements BeRequestEntity
{
    public function setRegionId($value)
    {
        return $this->with('region_id', $value);
    }

    public function setDistrictId($value)
    {
        return $this->with('district_id', $value);
    }

    public function setCityId($value)
    {
        return $this->with('city_id', $value);
    }

    public function setStreetName(string $value)
    {
        return $this->with('street_ua', $value);
    }
}
