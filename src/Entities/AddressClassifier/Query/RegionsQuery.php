<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Adonis
 * @category  Entries
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Adonis\Entities\AddressClassifier\Query;

use Adonis\Contracts\BeRequestEntity;

class RegionsQuery extends BaseQuery implements BeRequestEntity
{
    public function setRegionName(string $value)
    {
        return $this->with('region_name', $value);
    }

    public function setRegionNameEn(string $value)
    {
        return $this->with('region_name_en', $value);
    }
}
