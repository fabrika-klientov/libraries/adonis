<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Adonis
 * @category  Entries
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Adonis\Entities\AddressClassifier\Query;

use Adonis\Contracts\BeRequestEntity;

class AddressesQuery extends BaseQuery implements BeRequestEntity
{
    public function setStreetId($value)
    {
        return $this->with('street_id', $value);
    }

    public function setHouseNumber($value)
    {
        return $this->with('housenumber', $value);
    }
}
