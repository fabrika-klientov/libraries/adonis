<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Adonis
 * @category  Entries
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Adonis\Entities\AddressClassifier\Query;

use Adonis\Contracts\BeRequestEntity;

class CityDetailsQuery extends BaseQuery implements BeRequestEntity
{
    public function setPostCode($value)
    {
        return $this->with('postcode', $value);
    }

    public function setLang($value)
    {
        return $this->with('lang', $value);
    }
}
