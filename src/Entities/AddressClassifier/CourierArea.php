<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Adonis
 * @category  Entries
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Adonis\Entities\AddressClassifier;

use Adonis\Contracts\BeEntity;

/**
 * @property-read string $IS_COURIERAREA
 * @property-read string $postindex
 * */
class CourierArea extends Entity implements BeEntity
{
    public function isCourierArea(): bool
    {
        return $this->IS_COURIERAREA == 1;
    }
}
