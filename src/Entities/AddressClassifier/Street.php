<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Adonis
 * @category  Entries
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Adonis\Entities\AddressClassifier;

use Adonis\Contracts\BeEntity;

/**
 * @property-read string $REGION_ID
 * @property-read string $OLDSTREET_EN
 * @property-read string $DISTRICT_ID
 * @property-read string $CITY_RU
 * @property-read string $STREET_UA
 * @property-read string $SHORTSTREETTYPE_UA
 * @property-read string $DISTRICT_EN
 * @property-read string $REGION_EN
 * @property-read string $STREET_ID
 * @property-read string $STREETTYPE_UA
 * @property-read string $SHORTSTREETTYPE_RU
 * @property-read string $STREETTYPE_RU
 * @property-read string $OLDSTREET_UA
 * @property-read string $NEW_DISTRICT_UA
 * @property-read string $CITY_EN
 * @property-read string $STREET_EN
 * @property-read string $OLDSTREET_RU
 * @property-read string $REGION_RU
 * @property-read string $REGION_UA
 * @property-read string $SHORTSTREETTYPE_EN
 * @property-read string $STREETTYPE_EN
 * @property-read string $CITY_ID
 * @property-read string $DISTRICT_UA
 * @property-read string $STREET_RU
 * @property-read string $CITY_UA
 * @property-read string $DISTRICT_RU
 * */
class Street extends Entity implements BeEntity
{

}
