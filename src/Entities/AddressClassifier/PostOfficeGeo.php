<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Adonis
 * @category  Entries
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Adonis\Entities\AddressClassifier;

use Adonis\Contracts\BeEntity;

/**
 * @property-read string $CITYNAME
 * @property-read string $DISTANCE
 * @property-read string $LONGITUDE
 * @property-read string $ADDRESS
 * @property-read string $POSTINDEX
 * @property-read string $ID
 * @property-read string $LATITUDE
 * @property-read string $POSTFILIALNAME
 * */
class PostOfficeGeo extends Entity implements BeEntity
{

}
