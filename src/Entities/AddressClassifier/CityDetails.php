<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Adonis
 * @category  Entries
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Adonis\Entities\AddressClassifier;

use Adonis\Contracts\BeEntity;

/**
 * @property-read string $REGION_ID
 * @property-read string $DISTRICT_NAME
 * @property-read string $NEW_DISTRICT_NAME
 * @property-read string $CITY_ID
 * @property-read string $REGION_NAME
 * @property-read string $POSTCODE
 * @property-read string $DISTRICT_ID
 * @property-read string $CITY_NAME
 * @property-read string $CITYTYPE_NAME
 * @property-read string $OLDSTREET_NAME
 * @property-read string $OLDCITY_NAME
 * @property-read string $CITYTYPE_ID
 * */
class CityDetails extends Entity implements BeEntity
{

}
