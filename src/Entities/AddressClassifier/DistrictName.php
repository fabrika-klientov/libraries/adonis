<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Adonis
 * @category  Entries
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Adonis\Entities\AddressClassifier;

use Adonis\Contracts\BeEntity;

/**
 * @property-read string $REGION_ID
 * @property-read string $DISTRICT_NAME
 * @property-read string $NEW_DISTRICT_NAME
 * @property-read string $REGION_NAME
 * @property-read string $DISTRICT_ID
 * */
class DistrictName extends Entity implements BeEntity
{

}
