<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Adonis
 * @category  Entries
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Adonis\Entities\AddressClassifier;

use Adonis\Contracts\BeEntity;

/**
 * @property-read string $REGION_ID
 * @property-read string $POPULATION
 * @property-read string $DISTRICT_ID
 * @property-read string $LONGITUDE
 * @property-read string $CITY_KATOTTG
 * @property-read string $CITY_RU
 * @property-read string $DISTRICT_EN
 * @property-read string $REGION_EN
 * @property-read string $OLDCITY_RU
 * @property-read string $SHORTCITYTYPE_EN
 * @property-read string $CITYTYPE_UA
 * @property-read string $OLDCITY_UA
 * @property-read string $NEW_DISTRICT_UA
 * @property-read string $CITY_EN
 * @property-read string $CITYTYPE_RU
 * @property-read string $CITY_KOATUU
 * @property-read string $REGION_RU
 * @property-read string $NAME_UA
 * @property-read string $REGION_UA
 * @property-read string $OLDCITY_EN
 * @property-read string $SHORTCITYTYPE_RU
 * @property-read string $CITY_ID
 * @property-read string $DISTRICT_UA
 * @property-read string $CITYTYPE_EN
 * @property-read string $SHORTCITYTYPE_UA
 * @property-read string $LATTITUDE
 * @property-read string $CITY_UA
 * @property-read string $OWNOF
 * @property-read string $DISTRICT_RU
 * */
class City extends Entity implements BeEntity
{

}
