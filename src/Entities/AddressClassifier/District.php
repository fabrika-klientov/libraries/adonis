<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Adonis
 * @category  Entries
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Adonis\Entities\AddressClassifier;

use Adonis\Contracts\BeEntity;

/**
 * @property-read string $REGION_ID
 * @property-read string $DISTRICT_KOATUU
 * @property-read string $REGION_KATOTTG
 * @property-read string $DISTRICT_ID
 * @property-read string $REGION_RU
 * @property-read string $DISTRICT_KATOTTG
 * @property-read string $DISTRICT_EN
 * @property-read string $REGION_UA
 * @property-read string $REGION_EN
 * @property-read string $DISTRICT_UA
 * @property-read string $REGION_KOATUU
 * @property-read string $DISTRICT_RU
 * @property-read string $NEW_DISTRICT_UA
 * */
class District extends Entity implements BeEntity
{

}
