<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Adonis
 * @category  Entries
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Adonis\Entities\AddressClassifier;

use Adonis\Contracts\BeEntity;

/**
 * @property-read string $LOCK_EN
 * @property-read string $CITY_UA_VPZ
 * @property-read string $POSTTERMINAL
 * @property-read string $POSTOFFICE_UA
 * @property-read string $POSTCODE
 * @property-read string $ISAUTOMATED
 * @property-read string $PHONE
 * @property-read string $LONGITUDE
 * @property-read string $CITY_KATOTTG
 * @property-read string $STREET_UA_VPZ
 * @property-read string $POSTOFFICE_ID
 * @property-read string $POSTOFFICE_UA_DETAILS
 * @property-read string $LOCK_UA
 * @property-read string $CITY_UA_TYPE
 * @property-read string $CITY_VPZ_KATOTTG
 * @property-read string $LOCK_CODE
 * @property-read string $CITY_VPZ_ID
 * @property-read string $CITY_KOATUU
 * @property-read string $STREET_ID_VPZ
 * @property-read string $LOCK_RU
 * @property-read string $CITY_VPZ_KOATUU
 * @property-read string $TYPE_ACRONYM
 * @property-read string $TYPE_LONG
 * @property-read string $TYPE_ID
 * @property-read string $CITY_ID
 * @property-read string $HOUSENUMBER
 * @property-read string $LATTITUDE
 * @property-read string $CITY_UA
 * */
class PostOfficePostCode extends Entity implements BeEntity
{

}
