<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Adonis
 * @category  Entries
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Adonis\Entities\AddressClassifier;

use Adonis\Contracts\BeEntity;

/**
 * @property-read string $POLOCK_EN
 * @property-read string $TYPE_SHORT
 * @property-read string $CITY_RU
 * @property-read string $POREGION_ID
 * @property-read string $SHORTPDCITYTYPE_RU
 * @property-read string $DISTRICT_EN
 * @property-read string $PDCITYTYPE_EN
 * @property-read string $POSTINDEX
 * @property-read string $SHORTCITYTYPE_EN
 * @property-read string $MEREZA_NUMBER
 * @property-read string $POLOCK_UA
 * @property-read string $ID
 * @property-read string $STREETTYPE_RU
 * @property-read string $CITYTYPE_UA
 * @property-read string $POSTREET_ID
 * @property-read string $NEW_DISTRICT_UA
 * @property-read string $PO_LONG
 * @property-read string $PDCITY_UA
 * @property-read string $POLOCK_RU
 * @property-read string $PO_SHORT
 * @property-read string $LOCK_RU
 * @property-read string $TYPE_LONG
 * @property-read string $TYPE_ACRONYM
 * @property-read string $PARENT_ID
 * @property-read string $TECHINDEX
 * @property-read string $REGION_UA
 * @property-read string $PDCITY_ID
 * @property-read string $PDCITYTYPE_RU
 * @property-read string $IS_NODISTRICT
 * @property-read string $DISTRICT_UA
 * @property-read string $PO_CODE
 * @property-read string $ADDRESS
 * @property-read string $SHORTCITYTYPE_UA
 * @property-read string $PODISTRICT_ID
 * @property-read string $LOCK_EN
 * @property-read string $PDOLDCITYNAME_EN
 * @property-read string $POSTCODE
 * @property-read string $PDCITY_EN
 * @property-read string $PHONE
 * @property-read string $LONGITUDE
 * @property-read string $STREET_UA
 * @property-read string $REGION_EN
 * @property-read string $PDREGION_ID
 * @property-read string $SHORTPDCITYTYPE_UA
 * @property-read string $STREETTYPE_UA
 * @property-read string $LOCK_UA
 * @property-read string $ISVPZ
 * @property-read string $PDOLDCITYNAME_UA
 * @property-read string $CITY_EN
 * @property-read string $STREET_EN
 * @property-read string $LOCK_CODE
 * @property-read string $PDOLDCITYNAME_RU
 * @property-read string $CITYTYPE_RU
 * @property-read string $REGION_RU
 * @property-read string $POCITY_ID
 * @property-read string $PDCITY_RU
 * @property-read string $RESTRICTED_ACCESS
 * @property-read string $PDCITYTYPE_UA
 * @property-read string $SHORTCITYTYPE_RU
 * @property-read string $SHORTPDCITYTYPE_EN
 * @property-read string $STREETTYPE_EN
 * @property-read string $PDDISTRICT_ID
 * @property-read string $CITYTYPE_EN
 * @property-read string $HOUSENUMBER
 * @property-read string $LATTITUDE
 * @property-read string $STREET_RU
 * @property-read string $CITY_UA
 * @property-read string $DISTRICT_RU
 * */
class PostOffice extends Entity implements BeEntity
{

}
