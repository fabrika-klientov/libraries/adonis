<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Adonis
 * @category  Entries
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Adonis\Entities\Paginator\Query;

use Adonis\Contracts\BeRequestEntity;

class PageQuery extends BaseQuery implements BeRequestEntity
{
    public const ORDER_ASC = 'asc';
    public const ORDER_DESC = 'desc';

    public const STATUS_GROUP_ALL = 'ALL';
    public const STATUS_GROUP_NON_REGISTERED = 'NONREGISTERED';

    public function setSize($value)
    {
        return $this->with('size', $value);
    }

    public function setPage($value)
    {
        return $this->with('page', $value);
    }

    /**
     * Available::
     * declaredPrice, deliveryDate, deliveryPrice,
     * districtSortingCenter, lastModified, length, lifecycleLastModified,
     * lifecycleTime, height, packageType, postPay, postPayDeliveryPrice,
     * regionSortingCenter, weight, width
     * */
    public function setSort($value)
    {
        return $this->with('sort', $value);
    }

    public function setOrder($value)
    {
        return $this->with('order', $value);
    }

    public function setStatusesGroup($value)
    {
        return $this->with('statusesGroup', $value);
    }
}
