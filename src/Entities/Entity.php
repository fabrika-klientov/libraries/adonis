<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Adonis
 * @category  Entries
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Adonis\Entities;

use Adonis\Exceptions\AdonisException;
use Illuminate\Support\Collection;

abstract class Entity implements \JsonSerializable
{
    protected $data;

    public function __construct(array $data)
    {
        $this->data = $data;
    }

    /**
     * @param string $key
     * @param string $className
     * @param bool $isCollection
     * @return array|Collection
     *
     * @throws AdonisException
     */
    protected function collectFrom(string $key, string $className, bool $isCollection = false)
    {
        if (!class_exists($className)) {
            throw new AdonisException("Class name [$className] is not resolved");
        }

        $list = array_map(function ($item) use ($className) {
            return new $className($item);
        }, $this->{$key} ?? []);

        if ($isCollection) {
            return new Collection($list);
        }

        return $list;
    }

    public function __get($name)
    {
        return $this->data[$name] ?? null;
    }

    public function __set($name, $value)
    {
        $this->data[$name] = $value;
    }

    public function __isset($name)
    {
        return isset($this->data[$name]);
    }

    public function jsonSerialize()
    {
        return $this->data;
    }

    public function __toString()
    {
        return json_encode($this->data);
    }
}
