<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Adonis
 * @category  Entries
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Adonis\Entities\DocumentBack;

use Adonis\Contracts\BeEntity;
use Adonis\Entities\Shipments\StickerAddress;

/**
 * @property-read string $barcode
 * @property-read string $parentShipmentBarcode
 * @property-read string $senderName
 * @property-read string $senderLatinName
 * @property-read string $senderMiddleName
 * @property-read string $senderLastName
 * @property-read string $senderContactPersonName
 * @property-read string $senderPhone
 * @property-read bool $isIndividualSender
 * @property-read array $senderAddress
 * @property-read string $recipientName
 * @property-read string $recipientMiddleName
 * @property-read string $recipientLastName
 * @property-read string $recipientLatinName
 * @property-read string $recipientPhone
 * @property-read string $recipientContactPersonName
 * @property-read bool $isIndividualRecipient
 * @property-read array $recipientAddress
 * @property-read string $shipmentType
 * @property-read string $deliveryType
 * @property-read int $weight
 * @property-read int $length
 * @property-read string $quantity
 * @property-read string $number
 * @property-read bool $isPostPayPaidByRecipient
 * @property-read bool $isCheckOnDelivery
 * @property-read bool $isPaidByRecipient
 * @property-read string $declaredPrice
 * @property-read string $postPayDeliveryPrice
 * @property-read string $postPay
 * @property-read string $deliveryPrice
 * @property-read string $returnType
 * @property-read string $regionSortingCenter
 * @property-read string $districtSortingCenter
 * @property-read string $postOfficeNumber
 * @property-read string $postOfficeName
 * @property-read string $rejectionPostOffice
 * @property-read string $lastModified
 * @property-read int $index
 * */
class StickerDocumentBack extends Entity implements BeEntity
{
    /**
     * @return StickerAddress
     */
    public function senderAddress(): ?StickerAddress
    {
        return empty($this->senderAddress) ? null : new StickerAddress($this->senderAddress);
    }

    /**
     * @return StickerAddress
     */
    public function recipientAddress(): ?StickerAddress
    {
        return empty($this->recipientAddress) ? null : new StickerAddress($this->recipientAddress);
    }
}
