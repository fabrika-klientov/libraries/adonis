<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Adonis
 * @category  Entries
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Adonis\Entities\DocumentBack;

use Adonis\Contracts\BeEntity;
use Adonis\Entities\Shipments\StickerShipment;

/**
 * @property-read array $shipment
 * @property-read array $documentBack
 * */
class StickerShipmentWithStickerDocumentBack extends Entity implements BeEntity
{
    /**
     * @return StickerShipment|null
     */
    public function shipment(): ?StickerShipment
    {
        return empty($this->shipment) ? null : new StickerShipment($this->shipment);
    }

    /**
     * @return StickerDocumentBack|null
     */
    public function documentBack(): ?StickerDocumentBack
    {
        return empty($this->documentBack) ? null : new StickerDocumentBack($this->documentBack);
    }
}
