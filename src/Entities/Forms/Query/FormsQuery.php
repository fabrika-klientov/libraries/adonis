<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Adonis
 * @category  Entries
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Adonis\Entities\Forms\Query;

use Adonis\Contracts\BeRequestEntity;

class FormsQuery extends BaseQuery implements BeRequestEntity
{
    public const SIZE_A4 = 'SIZE_A4';
    public const SIZE_A5 = 'SIZE_A5';

    public function setSize($value)
    {
        return $this->with('size', $value);
    }

    public function setShowSenderName(bool $value)
    {
        return $this->with('showSenderName', $value);
    }

    public function setHideDeliveryPrice(bool $value)
    {
        return $this->with('hideDeliveryPrice', $value);
    }

    public function setHideDeclaredPrice(bool $value)
    {
        return $this->with('hideDeclaredPrice', $value);
    }

    public function setHidePostpayDeliveryPrice(bool $value)
    {
        return $this->with('hidePostpayDeliveryPrice', $value);
    }

    public function setHideWeight(bool $value)
    {
        return $this->with('hideWeight', $value);
    }
}
