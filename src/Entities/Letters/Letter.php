<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Adonis
 * @category  Entries
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Adonis\Entities\Letters;

use Adonis\Contracts\BeEntity;

/**
 * @property-read string $uuid
 * @property-read string $barcode
 * @property-read string $format
 * @property-read string $type
 * @property-read string $paymentType
 * @property-read int $weight
 * @property-read string $contract
 * @property-read array $counterparty
 * @property-read array $sender
 * @property-read array $senderAddress
 * @property-read array $senderPhone
 * @property-read array $recipient
 * @property-read array $recipientAddress
 * @property-read array $recipientPhone
 * @property-read string $externalId
 * @property-read string $price
 * @property-read string $vat
 * @property-read array $discounts
 * @property-read string $priceDescription
 * @property-read string $regionSortingCenter
 * @property-read string $postOfficeNumber
 * @property-read string $postOfficeName
 * @property-read int $printedCount
 * @property-read array $lifecycle
 * @property-read string $created
 * @property-read string $modified
 * @property-read bool $subpoena
 * @property-read bool $withDeliveryNotification
 * @property-read bool $personalHanding
 * @property-read bool $sms
 * */
class Letter extends Entity implements BeEntity
{
    /**
     * @return LetterCounterparty
     */
    public function counterparty(): ?LetterCounterparty
    {
        return empty($this->counterparty) ? null : new LetterCounterparty($this->counterparty);
    }

    /**
     * @return LetterClient
     */
    public function sender(): ?LetterClient
    {
        return empty($this->sender) ? null : new LetterClient($this->sender);
    }

    /**
     * @return LetterClient
     */
    public function recipient(): ?LetterClient
    {
        return empty($this->recipient) ? null : new LetterClient($this->recipient);
    }

    /**
     * @return LetterAddress
     */
    public function senderAddress(): ?LetterAddress
    {
        return empty($this->senderAddress) ? null : new LetterAddress($this->senderAddress);
    }

    /**
     * @return LetterAddress
     */
    public function recipientAddress(): ?LetterAddress
    {
        return empty($this->recipientAddress) ? null : new LetterAddress($this->recipientAddress);
    }

    /**
     * @return LetterPhone
     */
    public function senderPhone(): ?LetterPhone
    {
        return empty($this->senderPhone) ? null : new LetterPhone($this->senderPhone);
    }

    /**
     * @return LetterPhone
     */
    public function recipientPhone(): ?LetterPhone
    {
        return empty($this->recipientPhone) ? null : new LetterPhone($this->recipientPhone);
    }

    /**
     * @return LetterLifecycle
     */
    public function lifecycle(): ?LetterLifecycle
    {
        return empty($this->lifecycle) ? null : new LetterLifecycle($this->lifecycle);
    }

    /**
     * @return LetterDiscount[]
     */
    public function discounts(): array
    {
        return self::collectFrom('discounts', LetterDiscount::class);
    }
}
