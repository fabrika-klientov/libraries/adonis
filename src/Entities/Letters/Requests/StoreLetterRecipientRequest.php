<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Adonis
 * @category  Entries
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Adonis\Entities\Letters\Requests;

use Adonis\Contracts\BeRequestEntity;

class StoreLetterRecipientRequest extends BaseRequest implements BeRequestEntity
{
    public function uuid($value)
    {
        return $this->with('uuid', $value);
    }

    public function name($value)
    {
        return $this->with('name', $value);
    }

    public function type($value)
    {
        return $this->with('type', $value);
    }
}
