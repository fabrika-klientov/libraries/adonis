<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Adonis
 * @category  Entries
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Adonis\Entities\Letters\Requests;

use Adonis\Contracts\BeRequestEntity;

class StoreLetterRequest extends BaseRequest implements BeRequestEntity
{
    public const FORMAT_LETTER_114_X_162 = 'LETTER_114_X_162';
    public const FORMAT_LETTER_110_X_220 = 'LETTER_110_X_220';
    public const FORMAT_LETTER_162_X_229 = 'LETTER_162_X_229';
    public const FORMAT_LETTER_229_X_324 = 'LETTER_229_X_324';

    public const TYPE_SIMPLE = 'SIMPLE';
    public const TYPE_SIMPLE_PRIORITY = 'SIMPLE_PRIORITY';
    public const TYPE_RECOMMENDED = 'RECOMMENDED';
    public const TYPE_RECOMMENDED_PRIORITY = 'RECOMMENDED_PRIORITY';

    public const PAYMENT_TYPE_POSTAGE_STAMP = 'POSTAGE_STAMP';
    public const PAYMENT_TYPE_FRANKING_MACHINE = 'FRANKING_MACHINE';
    public const PAYMENT_TYPE_POSTAGE_PAID_IMPRINT = 'POSTAGE_PAID_IMPRINT';

    public function format($value)
    {
        return $this->with('format', $value);
    }

    public function type($value)
    {
        return $this->with('type', $value);
    }

    public function paymentType($value)
    {
        return $this->with('paymentType', $value);
    }

    public function weight($value)
    {
        return $this->with('weight', $value);
    }

    public function senderUuid($value)
    {
        return $this->with('senderUuid', $value);
    }

    public function senderAddressId($value)
    {
        return $this->with('senderAddressId', $value);
    }

    public function recipient(StoreLetterRecipientRequest $value)
    {
        return $this->with('recipient', $value);
    }

    public function recipientUuid($value)
    {
        return $this->with('recipientUuid', $value);
    }

    public function recipientAddressId($value)
    {
        return $this->with('recipientAddressId', $value);
    }

    public function sms(bool $value)
    {
        return $this->with('sms', $value);
    }

    public function withDeliveryNotification(bool $value)
    {
        return $this->with('withDeliveryNotification', $value);
    }

    public function personalHanding(bool $value)
    {
        return $this->with('personalHanding', $value);
    }

    public function externalId($value)
    {
        return $this->with('externalId', $value);
    }

    public function subpoena(bool $value)
    {
        return $this->with('subpoena', $value);
    }
}
