<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Adonis
 * @category  Entries
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Adonis\Entities\StatusTracking;

use Adonis\Contracts\BeEntity;

/**
 * @property-read string $from
 * @property-read string $to
 * */
class TrackingRoute extends Entity implements BeEntity
{

}
