<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Adonis
 * @category  Entries
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Adonis\Entities\StatusTracking;

use Adonis\Contracts\BeEntity;

/**
 * @property-read string $barcode
 * @property-read array $statuses
 * */
class TrackingCheckFoundItem extends Entity implements BeEntity
{
    /**
     * @return TrackingStatus[]
     */
    public function statuses(): array
    {
        return self::collectFrom('statuses', TrackingStatus::class);
    }
}
