<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Adonis
 * @category  Entries
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Adonis\Entities\StatusTracking;

use Adonis\Contracts\BeEntity;

/**
 * @property-read array $found
 * @property-read string[] $notFound
 * */
class TrackingCheck extends Entity implements BeEntity
{
    /**
     * @return TrackingCheckFoundItem[]
     */
    public function found()
    {
        $data = $this->found ?? [];

        return array_map(function ($item, $key) {
            return new TrackingCheckFoundItem(['barcode' => $key, 'statuses' => $item ?: []]);
        }, $data, array_keys($data));
    }
}
