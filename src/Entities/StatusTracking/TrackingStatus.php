<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Adonis
 * @category  Entries
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Adonis\Entities\StatusTracking;

use Adonis\Contracts\BeEntity;

/**
 * @property-read string $barcode
 * @property-read int $step
 * @property-read string $date
 * @property-read string $index
 * @property-read string $name
 * @property-read int $event
 * @property-read string $eventName
 * @property-read string $country
 * @property-read string $eventReason
 * @property-read int $eventReason_id
 * @property-read int $mailType
 * @property-read int $indexOrder
 * */
class TrackingStatus extends Entity implements BeEntity
{

}
