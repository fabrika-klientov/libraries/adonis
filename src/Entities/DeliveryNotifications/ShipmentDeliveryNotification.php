<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Adonis
 * @category  Entries
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Adonis\Entities\DeliveryNotifications;

use Adonis\Contracts\BeEntity;
use Adonis\Entities\Shipments\ShipmentLifecycle;

/**
 * @property-read string $uuid
 * @property-read string $barcode
 * @property-read string $parentType
 * @property-read array $lifecycle
 * @property-read string $firstPrintedTime
 * @property-read string $lastPrintedTime
 * @property-read int $printedCount
 * @property-read string $regionSortingCenter
 * @property-read string $districtSortingCenter
 * @property-read string $postOfficeNumber
 * @property-read string $postOfficeName
 * @property-read bool $subpoena
 * */
class ShipmentDeliveryNotification extends Entity implements BeEntity
{
    /**
     * @return ShipmentLifecycle|null
     */
    public function lifecycle(): ?ShipmentLifecycle
    {
        return empty($this->lifecycle) ? null : new ShipmentLifecycle($this->lifecycle);
    }
}
