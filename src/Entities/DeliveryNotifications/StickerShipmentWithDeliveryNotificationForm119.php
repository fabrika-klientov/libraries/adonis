<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Adonis
 * @category  Entries
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Adonis\Entities\DeliveryNotifications;

use Adonis\Contracts\BeEntity;
use Adonis\Entities\Shipments\StickerShipment;

/**
 * @property-read array $sticker
 * @property-read array $form119
 * */
class StickerShipmentWithDeliveryNotificationForm119 extends Entity implements BeEntity
{
    /**
     * @return StickerShipment|null
     */
    public function sticker(): ?StickerShipment
    {
        return empty($this->sticker) ? null : new StickerShipment($this->sticker);
    }

    /**
     * @return DeliveryNotificationForm119|null
     */
    public function form119(): ?DeliveryNotificationForm119
    {
        return empty($this->form119) ? null : new DeliveryNotificationForm119($this->form119);
    }
}
