<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Adonis
 * @category  Entries
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Adonis\Entities\DeliveryNotifications;

use Adonis\Contracts\BeEntity;

/**
 * @property-read string $barcode
 * @property-read string $parentBarcode
 * @property-read string $registrationTime
 * @property-read string $categoryOfDeparture
 * @property-read array $sender
 * @property-read array $recipient
 * @property-read array $sortingCenter
 * @property-read array $postOffice
 * @property-read string $index
 * */
class DeliveryNotificationForm119 extends Entity implements BeEntity
{
    /**
     * @return DeliveryNotificationForm119Client|null
     */
    public function sender(): ?DeliveryNotificationForm119Client
    {
        return empty($this->sender) ? null : new DeliveryNotificationForm119Client($this->sender);
    }

    /**
     * @return DeliveryNotificationForm119Client|null
     */
    public function recipient(): ?DeliveryNotificationForm119Client
    {
        return empty($this->recipient) ? null : new DeliveryNotificationForm119Client($this->recipient);
    }

    /**
     * @return DeliveryNotificationForm119SortingCenter|null
     */
    public function sortingCenter(): ?DeliveryNotificationForm119SortingCenter
    {
        return empty($this->sortingCenter) ? null : new DeliveryNotificationForm119SortingCenter($this->sortingCenter);
    }

    /**
     * @return DeliveryNotificationForm119PostOffice|null
     */
    public function postOffice(): ?DeliveryNotificationForm119PostOffice
    {
        return empty($this->postOffice) ? null : new DeliveryNotificationForm119PostOffice($this->postOffice);
    }
}
