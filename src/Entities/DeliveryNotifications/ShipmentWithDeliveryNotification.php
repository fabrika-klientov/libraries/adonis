<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Adonis
 * @category  Entries
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Adonis\Entities\DeliveryNotifications;

use Adonis\Contracts\BeEntity;
use Adonis\Entities\Shipments\Shipment;

/**
 * @property-read array $shipment
 * @property-read array $deliveryNotification
 * */
class ShipmentWithDeliveryNotification extends Entity implements BeEntity
{
    /**
     * @return Shipment|null
     */
    public function shipment(): ?Shipment
    {
        return empty($this->shipment) ? null : new Shipment($this->shipment);
    }

    /**
     * @return ShipmentDeliveryNotification|null
     */
    public function deliveryNotification(): ?ShipmentDeliveryNotification
    {
        return empty($this->deliveryNotification) ? null : new ShipmentDeliveryNotification($this->deliveryNotification);
    }
}
