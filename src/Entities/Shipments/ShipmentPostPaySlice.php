<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Adonis
 * @category  Entries
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Adonis\Entities\Shipments;

use Adonis\Contracts\BeEntity;

/**
 * @property-read string $uuid
 * @property-read array $recipient
 * @property-read float $dividedSum
 * @property-read string $created
 * @property-read string $postPayDeliveryDividePrice
 * */
class ShipmentPostPaySlice extends Entity implements BeEntity
{
    /**
     * @return ShipmentPostPaySliceAddress
     */
    public function recipient(): ?ShipmentPostPaySliceAddress
    {
        return empty($this->recipient) ? null : new ShipmentPostPaySliceAddress($this->recipient);
    }
}
