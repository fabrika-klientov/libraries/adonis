<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Adonis
 * @category  Entries
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Adonis\Entities\Shipments;

use Adonis\Contracts\BeEntity;

/**
 * @property-read string $uuid
 * @property-read int $orderNumber
 * @property-read string $clientUuid
 * @property-read string $type
 * @property-read int $addressId
 * @property-read int $phoneId
 * @property-read string $email
 * @property-read string $dropDate
 * @property-read string $interval
 * @property-read string[] $shipmentBarcodes
 * @property-read string[] $letterBarcodes
 * @property-read string $lastStatus
 * @property-read string $lastStatusDate
 *
 * @property-read string|null courierLastName (only for ShipmentsService::findCourierServiceOrders)
 * @property-read string|null $courierFirstName (only for ShipmentsService::findCourierServiceOrders)
 * @property-read string|null $courierMiddleName (only for ShipmentsService::findCourierServiceOrders)
 * @property-read string|null $courierPhoneNumber (only for ShipmentsService::findCourierServiceOrders)
 * @property-read string|null $carNumber (only for ShipmentsService::findCourierServiceOrders)
 * @property-read string|null $carType (only for ShipmentsService::findCourierServiceOrders)
 * */
class CourierServiceOrder extends Entity implements BeEntity
{

}
