<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Adonis
 * @category  Entries
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Adonis\Entities\Shipments;

use Adonis\Contracts\BeEntity;

/**
 * @property-read string $recipientName
 * @property-read string $number
 * @property-read float $sum
 * @property-read string $lastStatus
 * @property-read string $lastStatusNameUa
 * @property-read string $lastStatusNameEn
 * @property-read string $lastStatusNameRu
 * @property-read string $lastStatusTime
 * */
class ShipmentPostPayStatus extends Entity implements BeEntity
{

}
