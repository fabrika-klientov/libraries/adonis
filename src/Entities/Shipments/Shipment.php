<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Adonis
 * @category  Entries
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Adonis\Entities\Shipments;

use Adonis\Contracts\BeEntity;

/**
 * @property-read array $sender
 * @property-read array $recipient
 * @property-read string $deliveryType
 * @property-read array $parcels
 * @property-read string $type
 * @property-read int $senderAddressId
 * @property-read string $recipientPhone
 * @property-read string $recipientEmail
 * @property-read string $recipientAddressId
 * @property-read string $returnAddressId
 * @property-read string $shipmentGroupUuid
 * @property-read string $externalId
 * @property-read string $onFailReceiveType
 * @property-read string $postPay
 * @property-read string $description
 * @property-read bool $paidByRecipient
 * @property-read bool $bulky
 * @property-read bool $fragile
 * @property-read bool $bees
 * @property-read bool $sms
 * @property-read bool $postPayPaidByRecipient
 * @property-read bool $documentBack
 * @property-read bool $checkOnDelivery
 * @property-read bool $transferPostPayToBankAccount
 * @property-read bool $packedBySender
 * @property-read string $dropOffPostcode
 * @property-read int $weight
 * @property-read int $length
 * @property-read int $width
 * @property-read int $height
 * @property-read float $declaredPrice
 * @property-read bool $withDeliveryNotification
 * @property-read array $postPayDivide
 * @property-read bool $fittingAllowed
 * @property-read string $uuid
 * @property-read string $barcode
 * @property-read string $packageType
 * @property-read float $deliveryPrice
 * @property-read float $rawDeliveryPrice
 * @property-read string $currencyCode
 * @property-read float $postPayCurrencyCode
 * @property-read float $currencyExchangeRate
 * @property-read array $discounts
 * @property-read string $lastModified
 * @property-read array $lifecycle
 * @property-read string $status
 * @property-read float $postPayDeliveryPrice
 * @property-read string $calculationDescription
 * @property-read bool $deliveryPricePaid
 * @property-read bool $postPayPaid
 * @property-read bool $postPayDeliveryPricePaid
 * @property-read array $direction
 * @property-read array $transfer
 * @property-read bool $free
 * @property-read string $deliveryDate
 * @property-read float $postPayUah
 * @property-read bool $priceChangedInPostOffice
 * @property-read array $postPayRecipient
 * @property-read string $postPayRecipientUuid
 * @property-read string $internalIdentifier
 * @property-read float $returnDeliveryPrice
 * @property-read string[] $availablePaymentTypes
 * @property-read bool $paidByCardOnline
 * @property-read string $paidByCardPaymentId
 *
 * @property-read string $toReturnToSender
 * @property-read bool $personalHanding
 * @property-read bool $administrativeService
 * @property-read bool $listOfEnclosedItems
 * */
class Shipment extends Entity implements BeEntity
{
    /**
     * @return ShipmentClient
     */
    public function sender(): ?ShipmentClient
    {
        return empty($this->sender) ? null : new ShipmentClient($this->sender);
    }

    /**
     * @return ShipmentClient
     */
    public function recipient(): ?ShipmentClient
    {
        return empty($this->recipient) ? null : new ShipmentClient($this->recipient);
    }

    /**
     * @return ShipmentDiscount[]
     */
    public function discounts(): array
    {
        return self::collectFrom('discounts', ShipmentDiscount::class);
    }

    /**
     * @return ShipmentParcel[]
     */
    public function parcels(): array
    {
        return self::collectFrom('parcels', ShipmentParcel::class);
    }

    /**
     * @return ShipmentDirection|null
     */
    public function direction(): ?ShipmentDirection
    {
        return empty($this->direction) ? null : new ShipmentDirection($this->direction);
    }

    /**
     * @return ShipmentLifecycle|null
     */
    public function lifecycle(): ?ShipmentLifecycle
    {
        return empty($this->lifecycle) ? null : new ShipmentLifecycle($this->lifecycle);
    }

    /**
     * @return ShipmentPostPayRecipient|null
     */
    public function postPayRecipient(): ?ShipmentPostPayRecipient
    {
        return empty($this->postPayRecipient) ? null : new ShipmentPostPayRecipient($this->postPayRecipient);
    }

    /**
     * @return ShipmentTransfer|null
     */
    public function transfer(): ?ShipmentTransfer
    {
        return empty($this->transfer) ? null : new ShipmentTransfer($this->transfer);
    }

    /**
     * @return ShipmentPostPaySliceSimple[]
     */
    public function postPayDivide(): array
    {
        return self::collectFrom('postPayDivide', ShipmentPostPaySliceSimple::class);
    }
}
