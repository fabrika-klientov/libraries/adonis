<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Adonis
 * @category  Entries
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Adonis\Entities\Shipments;

use Adonis\Contracts\BeEntity;

/**
 * @property-read string $countryISO3166
 * @property-read string $countryName
 * @property-read string $countryNameEn
 * @property-read bool $countryIsLatinRequired
 * @property-read string $postcode
 * @property-read string $region
 * @property-read string $district
 * @property-read string $city
 * @property-read string $street
 * @property-read string $specialDestination
 * @property-read string $houseNumber
 * @property-read string $apartmentNumber
 * @property-read string $foreignStreetHouseApartment
 * @property-read string $mailbox
 * @property-read bool $main
 * */
class StickerAddress extends Entity implements BeEntity
{

}
