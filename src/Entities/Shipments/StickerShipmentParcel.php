<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Adonis
 * @category  Entries
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Adonis\Entities\Shipments;

use Adonis\Contracts\BeEntity;

/**
 * @property-read int $parcelNumber
 * @property-read string $barcode
 * @property-read string $contentsType
 * @property-read int $weight
 * @property-read int $length
 * @property-read int $width
 * @property-read int $height
 * @property-read float $declaredPrice
 * @property-read string $parcelItems
 * @property-read string $description
 * */
class StickerShipmentParcel extends Entity implements BeEntity
{

}
