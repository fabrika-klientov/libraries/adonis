<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Adonis
 * @category  Entries
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Adonis\Entities\Shipments;

use Adonis\Contracts\BeEntity;

/**
 * @property-read string $regionSortingCenter
 * @property-read string $districtSortingCenter
 * @property-read string $postOfficeNumber
 * @property-read string $postOfficeName
 * */
class ShipmentDirection extends Entity implements BeEntity
{

}
