<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Adonis
 * @category  Entries
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Adonis\Entities\Shipments\Requests;

use Adonis\Contracts\BeRequestEntity;

class StoreShipmentClientRequest extends BaseRequest implements BeRequestEntity
{
    public function uuid($value)
    {
        return $this->with('uuid', $value);
    }
}
