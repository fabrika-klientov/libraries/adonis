<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Adonis
 * @category  Entries
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Adonis\Entities\Shipments\Requests;

use Adonis\Contracts\BeRequestEntity;

class StoreShipmentPostPaySliceRequest extends BaseRequest implements BeRequestEntity
{
    public function recipientUuid($value)
    {
        return $this->with('recipientUuid', $value);
    }

    public function sum($value)
    {
        return $this->with('sum', $value);
    }

    public function postPayDeliveryDividePrice($value)
    {
        return $this->with('postPayDeliveryDividePrice', $value);
    }
}
