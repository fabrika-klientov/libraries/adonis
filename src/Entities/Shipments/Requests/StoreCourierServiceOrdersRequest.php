<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Adonis
 * @category  Entries
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Adonis\Entities\Shipments\Requests;

use Adonis\Contracts\BeRequestEntity;

class StoreCourierServiceOrdersRequest extends BaseRequest implements BeRequestEntity
{
    public const TYPE_SINGLE = 'SINGLE';
    public const TYPE_MASS = 'MASS';

    public const INTERVAL_09_12 = 'INTERVAL_09_12';
    public const INTERVAL_12_15 = 'INTERVAL_12_15';
    public const INTERVAL_15_18 = 'INTERVAL_15_18';

    public const STATUS_ORDERED = 'ORDERED';
    public const STATUS_TO_PERFORM = 'TO_PERFORM';
    public const STATUS_TRANSFERRED_TO_COURIER = 'TRANSFERRED_TO_COURIER';
    public const STATUS_DONE = 'DONE';
    public const STATUS_POSTPONED = 'POSTPONED';
    public const STATUS_DENIED = 'DENIED';
    public const STATUS_NOT_DELIVERED = 'NOT_DELIVERED';

    public function clientUuid($value)
    {
        return $this->with('clientUuid', $value);
    }

    public function type($value)
    {
        return $this->with('type', $value);
    }

    public function addressId($value)
    {
        return $this->with('addressId', $value);
    }

    public function phoneId($value)
    {
        return $this->with('phoneId', $value);
    }

    public function dropDate($value)
    {
        return $this->with('dropDate', $value);
    }

    public function interval($value)
    {
        return $this->with('interval', $value);
    }

    /**
     * @param string[] $value
     */
    public function shipmentBarcodes(array $value)
    {
        return $this->with('shipmentBarcodes', $value);
    }

    public function letterBarcodes(array $value)
    {
        return $this->with('letterBarcodes', $value);
    }

    public function email($value)
    {
        return $this->with('email', $value);
    }
}
