<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Adonis
 * @category  Entries
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Adonis\Entities\Shipments\Requests;

trait Shipments
{
    public function deliveryType($value)
    {
        return $this->with('deliveryType', $value);
    }

    public function type($value)
    {
        return $this->with('type', $value);
    }

    public function senderAddressId($value)
    {
        return $this->with('senderAddressId', $value);
    }

    public function recipientPhone($value)
    {
        return $this->with('recipientPhone', $value);
    }

    public function recipientEmail($value)
    {
        return $this->with('recipientEmail', $value);
    }

    public function recipientAddressId($value)
    {
        return $this->with('recipientAddressId', $value);
    }

    public function returnAddressId($value)
    {
        return $this->with('returnAddressId', $value);
    }

    public function externalId($value)
    {
        return $this->with('externalId', $value);
    }

    public function onFailReceiveType($value)
    {
        return $this->with('onFailReceiveType', $value);
    }

    public function postPay($value)
    {
        return $this->with('postPay', $value);
    }

    public function description($value)
    {
        return $this->with('description', $value);
    }

    public function paidByRecipient(bool $value)
    {
        return $this->with('paidByRecipient', $value);
    }

    public function bulky(bool $value)
    {
        return $this->with('bulky', $value);
    }

    public function fragile(bool $value)
    {
        return $this->with('fragile', $value);
    }

    public function bees(bool $value)
    {
        return $this->with('bees', $value);
    }

    public function sms(bool $value)
    {
        return $this->with('sms', $value);
    }

    public function postPayPaidByRecipient(bool $value)
    {
        return $this->with('postPayPaidByRecipient', $value);
    }

    public function documentBack(bool $value)
    {
        return $this->with('documentBack', $value);
    }

    public function checkOnDelivery(bool $value)
    {
        return $this->with('checkOnDelivery', $value);
    }

    public function transferPostPayToBankAccount(bool $value)
    {
        return $this->with('transferPostPayToBankAccount', $value);
    }

    public function packedBySender(bool $value)
    {
        return $this->with('packedBySender', $value);
    }

    public function dropOffPostcode($value)
    {
        return $this->with('dropOffPostcode', $value);
    }

    public function withDeliveryNotification(bool $value)
    {
        return $this->with('withDeliveryNotification', $value);
    }

    public function postPayRecipientUuid($value)
    {
        return $this->with('postPayRecipientUuid', $value);
    }
}
