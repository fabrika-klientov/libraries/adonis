<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Adonis
 * @category  Entries
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Adonis\Entities\Shipments\Requests;

use Adonis\Contracts\BeRequestEntity;

class StoreShipmentRequest extends BaseRequest implements BeRequestEntity
{
    use Shipments;

    public const DELIVERY_TYPE_D2W = 'D2W';
    public const DELIVERY_TYPE_D2D = 'D2D';
    public const DELIVERY_TYPE_W2D = 'W2D';
    public const DELIVERY_TYPE_W2W = 'W2W';

    public const TYPE_EXPRESS = 'EXPRESS';
    public const TYPE_STANDARD = 'STANDARD';
    public const TYPE_DOCUMENT = 'DOCUMENT';

    public const RECEIVE_TYPE_RETURN = 'RETURN';
    public const RECEIVE_TYPE_RETURN_AFTER_7_DAYS = 'RETURN_AFTER_7_DAYS';
    public const RECEIVE_TYPE_PROCESS_AS_REFUSAL = 'PROCESS_AS_REFUSAL';

    public const STATUS_CREATED = 'CREATED';
    public const STATUS_REGISTERED = 'REGISTERED';
    public const STATUS_DELIVERED = 'DELIVERED';
    public const STATUS_IN_DEPARTMENT = 'IN_DEPARTMENT';
    public const STATUS_DELIVERING = 'DELIVERING';
    public const STATUS_FORWARDING = 'FORWARDING';
    public const STATUS_RETURNING = 'RETURNING';
    public const STATUS_RETURNED = 'RETURNED';
    public const STATUS_STORAGE = 'STORAGE';
    public const STATUS_CANCELED = 'CANCELED';
    public const STATUS_DELETED = 'DELETED';

    public const PAYMENT_TYPE_CASH = 'CASH';
    public const PAYMENT_TYPE_CASHLESS = 'CASHLESS';
    public const PAYMENT_TYPE_CARD = 'CARD';
    public const PAYMENT_TYPE_CARD_ON_SITE = 'CARD_ON_SITE';

    public function sender(StoreShipmentClientRequest $value)
    {
        return $this->with('sender', $value);
    }

    public function recipient(StoreShipmentClientRequest $value)
    {
        return $this->with('recipient', $value);
    }

    /**
     * @param StoreShipmentParcelRequest[] $value
     */
    public function parcels(array $value)
    {
        return $this->with('parcels', $value);
    }

    /**
     * @param StoreShipmentPostPaySliceRequest[] $value
     */
    public function postPayDivide(array $value)
    {
        return $this->with('postPayDivide', $value);
    }

    // Only for type DOCUMENT

    public function personalHanding(bool $value)
    {
        return $this->with('personalHanding', $value);
    }

    public function administrativeService(bool $value)
    {
        return $this->with('administrativeService', $value);
    }

    /**
     * @param StoreShipmentParcelItemRequest[] $value
     */
    public function parcelItems(array $value)
    {
        return $this->with('parcelItems', $value);
    }
}
