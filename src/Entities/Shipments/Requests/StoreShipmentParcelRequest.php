<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Adonis
 * @category  Entries
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Adonis\Entities\Shipments\Requests;

use Adonis\Contracts\BeRequestEntity;

class StoreShipmentParcelRequest extends BaseRequest implements BeRequestEntity
{
    public function name($value)
    {
        return $this->with('name', $value);
    }

    public function weight($value)
    {
        return $this->with('weight', $value);
    }

    public function length($value)
    {
        return $this->with('length', $value);
    }

    public function declaredPrice($value)
    {
        return $this->with('declaredPrice', $value);
    }

    public function description($value)
    {
        return $this->with('description', $value);
    }

    public function parcelNumber($value)
    {
        return $this->with('parcelNumber', $value);
    }

    public function barcode($value)
    {
        return $this->with('barcode', $value);
    }

    public function width($value)
    {
        return $this->with('width', $value);
    }

    public function height($value)
    {
        return $this->with('height', $value);
    }

    public function parcelItems($value)
    {
        return $this->with('parcelItems', $value);
    }
}
