<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Adonis
 * @category  Entries
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Adonis\Entities\Shipments\Requests;

use Adonis\Contracts\BeRequestEntity;

class UpdateShipmentRequest extends BaseRequest implements BeRequestEntity
{
    use Shipments;

    public function uuid($value)
    {
        return $this->with('uuid', $value);
    }

    public function sender(UpdateShipmentClientRequest $value)
    {
        return $this->with('sender', $value);
    }

    public function recipient(UpdateShipmentClientRequest $value)
    {
        return $this->with('recipient', $value);
    }

    /**
     * @param UpdateShipmentParcelRequest[] $value
     */
    public function parcels(array $value)
    {
        return $this->with('parcels', $value);
    }

    /**
     * @param UpdateShipmentPostPaySliceRequest[] $value
     */
    public function postPayDivide(array $value)
    {
        return $this->with('postPayDivide', $value);
    }
}
