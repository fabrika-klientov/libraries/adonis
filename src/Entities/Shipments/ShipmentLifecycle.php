<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Adonis
 * @category  Entries
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Adonis\Entities\Shipments;

use Adonis\Contracts\BeEntity;

/**
 * @property-read string $status
 * @property-read string $statusDate
 *
 * @property-read string $shipmentUuid (for ShipmentsService::getLifecycle)
 * @property-read string $shipmentBarcode (for ShipmentsService::getLifecycle)
 * */
class ShipmentLifecycle extends Entity implements BeEntity
{

}
