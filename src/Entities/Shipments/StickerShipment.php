<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Adonis
 * @category  Entries
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Adonis\Entities\Shipments;

use Adonis\Contracts\BeEntity;

/**
 * @property-read string $uuid
 * @property-read string $type
 * @property-read string $senderName
 * @property-read string $senderMiddleName
 * @property-read string $senderLastName
 * @property-read string $senderLatinName
 * @property-read string $senderCounterpartyName
 * @property-read array $senderAddresses
 * @property-read array $senderPhones
 * @property-read bool $isIndividualSender
 * @property-read string $senderEdrpou
 * @property-read string $senderBankCode
 * @property-read string $senderBankAccount
 * @property-read string $senderBankName
 * @property-read string $senderTin
 * @property-read string $senderContactPersonName
 * @property-read string $senderEmail
 * @property-read string $recipientName
 * @property-read string $recipientLatinName
 * @property-read array $recipientAddresses
 * @property-read array $recipientPhones
 * @property-read bool $isIndividualRecipient
 * @property-read string $recipientContactPersonName
 * @property-read string $recipientPhone
 * @property-read string $recipientEmail
 * @property-read array $senderAddress
 * @property-read array $recipientAddress
 * @property-read array $returnAddress
 * @property-read array $postPayRecipient
 * @property-read array $postPayDivided
 * @property-read string $externalId
 * @property-read string $deliveryType
 * @property-read string $packageType
 * @property-read string $onFailReceiveType
 * @property-read string $barcode
 * @property-read int $weight
 * @property-read int $length
 * @property-read int $width
 * @property-read int $height
 * @property-read float $declaredPrice
 * @property-read float $deliveryPrice
 * @property-read float $deliveryPriceUa
 * @property-read float $postPay
 * @property-read float $postPayDeliveryPrice
 * @property-read string $postPayCurrencyCode
 * @property-read string $lastModified
 * @property-read string $description
 * @property-read array $parcels
 * @property-read array $direction
 * @property-read string $isPaidByRecipient
 * @property-read bool $isPostPayPaidByRecipient
 * @property-read bool $isBulky
 * @property-read bool $isFragile
 * @property-read bool $isBees
 * @property-read bool $isRecommended
 * @property-read bool $isSms
 * @property-read bool $isDocumentBack
 * @property-read string $documentBackBarcode
 * @property-read bool $isCheckOnDelivery
 * @property-read bool $isTransferPostPayToBankAccount
 * @property-read bool $isWithDeliveryNotification
 * @property-read bool $isAdministrativeService
 * @property-read bool $isListOfEnclosedItems
 * @property-read bool $isPersonalHanding
 * @property-read bool $isFree
 * @property-read string $internationalData
 * @property-read string $defaultCurrencyNaming
 * @property-read string $tmmBarcode
 * @property-read string $usaAirport
 * @property-read string $categoryInvoice
 * @property-read string $index
 * */
class StickerShipment extends Entity implements BeEntity
{
    /**
     * @return StickerAddress[]
     */
    public function senderAddresses(): array
    {
        return self::collectFrom('senderAddresses', StickerAddress::class);
    }

    /**
     * @return StickerAddress
     */
    public function senderAddress(): ?StickerAddress
    {
        return empty($this->senderAddress) ? null : new StickerAddress($this->senderAddress);
    }

    /**
     * @return StickerAddress[]
     */
    public function recipientAddresses(): array
    {
        return self::collectFrom('recipientAddresses', StickerAddress::class);
    }

    /**
     * @return StickerAddress
     */
    public function recipientAddress(): ?StickerAddress
    {
        return empty($this->recipientAddress) ? null : new StickerAddress($this->recipientAddress);
    }

    /**
     * @return StickerAddress
     */
    public function returnAddress(): ?StickerAddress
    {
        return empty($this->returnAddress) ? null : new StickerAddress($this->returnAddress);
    }

    /**
     * @return StickerShipmentPhone[]
     */
    public function senderPhones(): array
    {
        return self::collectFrom('senderPhones', StickerShipmentPhone::class);
    }

    /**
     * @return StickerShipmentPhone[]
     */
    public function recipientPhones(): array
    {
        return self::collectFrom('recipientPhones', StickerShipmentPhone::class);
    }

    /**
     * @return ShipmentPostPayRecipient|null
     */
    public function postPayRecipient(): ?ShipmentPostPayRecipient
    {
        return empty($this->postPayRecipient) ? null : new ShipmentPostPayRecipient($this->postPayRecipient);
    }

    /**
     * @return ShipmentPostPaySliceSimple[]
     */
    public function postPayDivided(): array
    {
        return self::collectFrom('postPayDivided', ShipmentPostPaySliceSimple::class);
    }

    /**
     * @return StickerShipmentParcel[]
     */
    public function parcels(): array
    {
        return self::collectFrom('parcels', StickerShipmentParcel::class);
    }

    /**
     * @return ShipmentDirection|null
     */
    public function direction(): ?ShipmentDirection
    {
        return empty($this->direction) ? null : new ShipmentDirection($this->direction);
    }
}
