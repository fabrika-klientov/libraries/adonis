<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Adonis
 * @category  Entries
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Adonis\Entities\ShipmentGroups;

/**
 * @property-read string $targetUuid
 * @property-read string $targetBarcode (one of)
 * @property-read int $httpStatus
 * */
class ShipmentGroupUpdatedMessage extends Message
{

}
