<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Adonis
 * @category  Entries
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Adonis\Entities\ShipmentGroups;

use Adonis\Contracts\BeEntity;

/**
 * @property-read string $uuid
 * @property-read string $name
 * @property-read string $clientUuid
 * @property-read string $type
 * @property-read string $created
 * @property-read bool $closed
 * */
class ShipmentGroup extends Entity implements BeEntity
{

}
