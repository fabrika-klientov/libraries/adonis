<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Adonis
 * @category  Entries
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Adonis\Entities\ShipmentGroups\Requests;

use Adonis\Contracts\BeRequestEntity;

class StoreShipmentGroupRequest extends BaseRequest implements BeRequestEntity
{
    public const TYPE_EXPRESS = 'EXPRESS';
    public const TYPE_STANDARD = 'STANDARD';
    public const TYPE_DOCUMENT = 'DOCUMENT';

    public function name($value)
    {
        return $this->with('name', $value);
    }

    public function clientUuid($value)
    {
        return $this->with('clientUuid', $value);
    }

    public function type($value)
    {
        return $this->with('type', $value);
    }
}
