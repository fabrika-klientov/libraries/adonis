<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Adonis
 * @category  Entries
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Adonis\Entities;

abstract class BaseRequest implements \JsonSerializable
{
    protected $data = [];

    public function __construct(array $data = [])
    {
        $this->data = $data;
    }

    public function getData(): array
    {
        return $this->data;
    }

    protected function with(string $key, $value)
    {
        $this->set($key, $value);

        return $this;
    }

    protected function set(string $key, $value)
    {
        $this->data[$key] = $value;
    }

    protected function remove(string $key)
    {
        unset($this->data[$key]);
    }

    /** TODO: If API will be stability -> realize this method
     */
    public function validate(): bool
    {
        return true;
    }

    public function __toString()
    {
        return json_encode($this->data);
    }

    public function jsonSerialize()
    {
        return $this->data;
    }
}
