<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Adonis
 * @category  Entries
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Adonis\Entities\Clients;

use Adonis\Contracts\BeEntity;

/**
 * @property-read string $uuid
 * @property-read array $recipient
 * @property-read string $created
 * */
class ClientPostPayRecipient extends Entity implements BeEntity
{
    public function recipient(): ?PostPayRecipient
    {
        return empty($this->recipient) ? null : new PostPayRecipient($this->recipient);
    }
}
