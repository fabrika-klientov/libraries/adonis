<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Adonis
 * @category  Entries
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Adonis\Entities\Clients;

use Adonis\Contracts\BeEntity;

/**
 * @property-read string $uuid
 * @property-read int $addressId
 * @property-read array $address
 * @property-read string $type
 * @property-read bool $main
 * */
class ClientAddress extends Entity implements BeEntity
{
    public function address(): ?Address
    {
        return empty($this->address) ? null : new Address($this->address);
    }
}
