<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Adonis
 * @category  Entries
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Adonis\Entities\Clients;

use Adonis\Contracts\BeEntity;

/**
 * @property-read int $id
 * @property-read string $postcode
 * @property-read string $region
 * @property-read string $district
 * @property-read string $city
 * @property-read string $street
 * @property-read string $houseNumber
 * @property-read string $apartmentNumber
 * @property-read string $description
 * @property-read bool $countryside
 * @property-read bool $posteRestante
 * @property-read string $foreignStreetHouseApartment
 * @property-read string $detailedInfo
 * @property-read string $created
 * @property-read string $lastModified
 * @property-read string $country
 * */
class Address extends Entity implements BeEntity
{

}
