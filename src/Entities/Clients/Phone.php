<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Adonis
 * @category  Entries
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Adonis\Entities\Clients;

use Adonis\Contracts\BeEntity;

/**
 * @property-read int $id
 * @property-read string $rawNumber
 * @property-read string $phoneNumber
 * @property-read string $countryCode
 * @property-read string $providerCode
 * @property-read string $localCode
 * @property-read bool $normalized
 * @property-read string $country
 * */
class Phone extends Entity implements BeEntity
{

}
