<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Adonis
 * @category  Entries
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Adonis\Entities\Clients;

use Adonis\Contracts\BeEntity;

/**
 * @property-read string $uuid
 * @property-read string $email
 * @property-read bool $main
 * */
class ClientEmail extends Entity implements BeEntity
{

}
