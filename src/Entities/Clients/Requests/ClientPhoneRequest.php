<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Adonis
 * @category  Entries
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Adonis\Entities\Clients\Requests;

use Adonis\Contracts\BeRequestEntity;

class ClientPhoneRequest extends BaseRequest implements BeRequestEntity
{
    public function phoneId($value)
    {
        return $this->with('phoneId', $value);
    }

    public function phoneNumber($value)
    {
        return $this->with('phoneNumber', $value);
    }

    public function type($value)
    {
        return $this->with('type', $value);
    }

    public function main(bool $value)
    {
        return $this->with('main', $value);
    }
}
