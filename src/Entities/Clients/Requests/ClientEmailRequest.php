<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Adonis
 * @category  Entries
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Adonis\Entities\Clients\Requests;

use Adonis\Contracts\BeRequestEntity;

class ClientEmailRequest extends BaseRequest implements BeRequestEntity
{
    public function email($value)
    {
        return $this->with('email', $value);
    }

    public function main(bool $value)
    {
        return $this->with('main', $value);
    }
}
