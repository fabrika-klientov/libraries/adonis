<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Adonis
 * @category  Entries
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Adonis\Entities\Clients\Requests;

trait Clients
{
    public function name($value)
    {
        return $this->with('name', $value);
    }

    public function firstName($value)
    {
        return $this->with('firstName', $value);
    }

    public function lastName($value)
    {
        return $this->with('lastName', $value);
    }

    public function addressId($value)
    {
        return $this->with('addressId', $value);
    }

    public function edrpou($value)
    {
        return $this->with('edrpou', $value);
    }

    public function tin($value)
    {
        return $this->with('tin', $value);
    }

    public function phoneNumber($value)
    {
        return $this->with('phoneNumber', $value);
    }

    public function latinName($value)
    {
        return $this->with('latinName', $value);
    }

    public function type($value)
    {
        return $this->with('type', $value);
    }

    public function middleName($value)
    {
        return $this->with('middleName', $value);
    }

    public function uniqueRegistrationNumber($value)
    {
        return $this->with('uniqueRegistrationNumber', $value);
    }

    /**
     * @param ClientAddressRequest[] $value
     */
    public function addresses(array $value)
    {
        return $this->with('addresses', $value);
    }

    /**
     * @param ClientPhoneRequest[] $value
     */
    public function phones(array $value)
    {
        return $this->with('phones', $value);
    }

    public function bankAccount($value)
    {
        return $this->with('bankAccount', $value);
    }

    public function email($value)
    {
        return $this->with('email', $value);
    }

    /**
     * @param ClientEmailRequest[] $value
     */
    public function emails(array $value)
    {
        return $this->with('emails', $value);
    }

    public function externalId($value)
    {
        return $this->with('externalId', $value);
    }

    public function contactPersonName($value)
    {
        return $this->with('contactPersonName', $value);
    }

    public function resident($value)
    {
        return $this->with('resident', $value);
    }
}
