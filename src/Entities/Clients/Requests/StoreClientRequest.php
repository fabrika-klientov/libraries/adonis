<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Adonis
 * @category  Entries
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Adonis\Entities\Clients\Requests;

use Adonis\Contracts\BeRequestEntity;

class StoreClientRequest extends BaseRequest implements BeRequestEntity
{
    use Clients;

    public const INDIVIDUAL = 'INDIVIDUAL';
    public const COMPANY = 'COMPANY';
    public const PRIVATE_ENTREPRENEUR = 'PRIVATE_ENTREPRENEUR';
}
