<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Adonis
 * @category  Entries
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Adonis\Entities\Clients;

use Adonis\Contracts\BeEntity;

/**
 * @property-read string $uuid
 * @property-read string $name
 * @property-read string $firstName
 * @property-read string $middleName
 * @property-read string $lastName
 * @property-read string $latinName
 * @property-read string $contactPersonName
 * @property-read string $type
 * @property-read string $postId
 * @property-read string $externalId
 * @property-read string $uniqueRegistrationNumber
 * @property-read string $counterpartyUuid
 * @property-read int $addressId
 * @property-read array $addresses
 * @property-read string $phoneNumber
 * @property-read array $phones
 * @property-read string $email
 * @property-read array $emails
 * @property-read string $bankAccount
 * @property-read string $postPayPaymentType
 * @property-read array $accountType
 * @property-read array $postPayRecipients
 * @property-read bool $resident
 * @property-read bool $GDPRRead
 * @property-read bool $GDPRAccept
 * @property-read bool $personalDataApproved
 * @property-read bool $checkOnDeliveryAllowed
 * */
class Client extends Entity implements BeEntity
{
    /**
     * @return ClientAddress[]
     */
    public function addresses(): array
    {
        return self::collectFrom('addresses', ClientAddress::class);
    }

    /**
     * @return ClientPhone[]
     */
    public function phones(): array
    {
        return self::collectFrom('phones', ClientPhone::class);
    }

    /**
     * @return ClientEmail[]
     */
    public function emails(): array
    {
        return self::collectFrom('emails', ClientEmail::class);
    }

    /**
     * @return PostPayRecipient[]
     */
    public function postPayRecipients(): array
    {
        return self::collectFrom('postPayRecipients', PostPayRecipient::class);
    }
}
