<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Adonis
 * @category  Entries
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Adonis\Entities\Addresses\Requests;

use Adonis\Contracts\BeRequestEntity;

class StoreAddressRequest extends BaseRequest implements BeRequestEntity
{
    public function postcode($value)
    {
        return $this->with('postcode', $value);
    }

    public function region($value)
    {
        return $this->with('region', $value);
    }

    public function district($value)
    {
        return $this->with('district', $value);
    }

    public function city($value)
    {
        return $this->with('city', $value);
    }

    public function street($value)
    {
        return $this->with('street', $value);
    }

    public function houseNumber($value)
    {
        return $this->with('houseNumber', $value);
    }

    public function apartmentNumber($value)
    {
        return $this->with('apartmentNumber', $value);
    }

    public function description($value)
    {
        return $this->with('description', $value);
    }

    public function specialDestination($value)
    {
        return $this->with('specialDestination', $value);
    }

    public function mailbox($value)
    {
        return $this->with('mailbox', $value);
    }

    public function posteRestante($value)
    {
        return $this->with('posteRestante', $value);
    }
}
