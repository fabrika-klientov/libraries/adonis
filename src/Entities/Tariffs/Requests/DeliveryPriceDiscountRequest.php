<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Adonis
 * @category  Entries
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Adonis\Entities\Tariffs\Requests;

use Adonis\Contracts\BeRequestEntity;

class DeliveryPriceDiscountRequest extends BaseRequest implements BeRequestEntity
{
    public function description($value)
    {
        return $this->with('description', $value);
    }

    public function rate($value)
    {
        return $this->with('rate', $value);
    }
}
