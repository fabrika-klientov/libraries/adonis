<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Adonis
 * @category  Entries
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Adonis\Entities\Tariffs\Requests;

use Adonis\Contracts\BeRequestEntity;

class DeliveryPriceRequest extends BaseRequest implements BeRequestEntity
{
    public const TYPE_EXPRESS = 'EXPRESS';
    public const TYPE_STANDARD = 'STANDARD';

    public const DELIVERY_TYPE_D2W = 'D2W';
    public const DELIVERY_TYPE_D2D = 'D2D';
    public const DELIVERY_TYPE_W2D = 'W2D';
    public const DELIVERY_TYPE_W2W = 'W2W';

    public function weight($value)
    {
        return $this->with('weight', $value);
    }

    public function length($value)
    {
        return $this->with('length', $value);
    }

    public function addressFrom(DeliveryPriceAddressRequest $value)
    {
        return $this->with('addressFrom', $value);
    }

    public function postcode($value)
    {
        return $this->with('postcode', $value);
    }

    public function addressTo(DeliveryPriceAddressRequest $value)
    {
        return $this->with('addressTo', $value);
    }

    public function type($value)
    {
        return $this->with('type', $value);
    }

    public function deliveryType($value)
    {
        return $this->with('deliveryType', $value);
    }

    public function width($value)
    {
        return $this->with('width', $value);
    }

    public function height($value)
    {
        return $this->with('height', $value);
    }

    /**
     * This method for "validate" (validate method is reserved)
     * */
    public function validat(bool $value)
    {
        return $this->with('validate', $value);
    }

    public function declaredPrice($value)
    {
        return $this->with('declaredPrice', $value);
    }

    /**
     * @param DeliveryPriceDiscountRequest[] $value
     */
    public function discounts(array $value)
    {
        return $this->with('discounts', $value);
    }

    public function sms(bool $value)
    {
        return $this->with('sms', $value);
    }

    public function documentBack(bool $value)
    {
        return $this->with('documentBack', $value);
    }

    public function withDeliveryNotification(bool $value)
    {
        return $this->with('withDeliveryNotification', $value);
    }

    public function lengthOverpayRatio($value)
    {
        return $this->with('lengthOverpayRatio', $value);
    }

    public function documentBackDeliveryType($value)
    {
        return $this->with('documentBackDeliveryType', $value);
    }

    /**
     * @param DeliveryPriceParcelRequest[] $value
     */
    public function parcels(array $value)
    {
        return $this->with('parcels', $value);
    }
}
