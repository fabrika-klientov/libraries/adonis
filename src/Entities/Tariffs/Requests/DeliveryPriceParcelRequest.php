<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Adonis
 * @category  Entries
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Adonis\Entities\Tariffs\Requests;

use Adonis\Contracts\BeRequestEntity;

class DeliveryPriceParcelRequest extends BaseRequest implements BeRequestEntity
{
    public function weight($value)
    {
        return $this->with('weight', $value);
    }

    public function length($value)
    {
        return $this->with('length', $value);
    }
}
