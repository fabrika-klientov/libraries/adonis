<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Adonis
 * @category  Entries
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Adonis\Entities\Tariffs;

use Adonis\Contracts\BeEntity;

/**
 * @property-read string $uuid
 * @property-read int $weight
 * @property-read int $length
 * @property-read string $type
 * @property-read string $w2wVariation
 * @property-read float $price
 * @property-read float $pricePerKg
 * */
class Tariff extends Entity implements BeEntity
{

}
