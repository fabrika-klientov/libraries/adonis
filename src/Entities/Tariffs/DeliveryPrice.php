<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Adonis
 * @category  Entries
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Adonis\Entities\Tariffs;

use Adonis\Contracts\BeEntity;

/**
 * @property-read float $deliveryPrice
 * @property-read float $rawDeliveryPrice
 * @property-read string $calculationDescription
 * @property-read array $parcels
 * @property-read bool $validate
 * @property-read array $addressFrom
 * @property-read array $addressTo
 * @property-read string $type
 * @property-read string $deliveryType
 * @property-read int $weight
 * @property-read int $length
 * @property-read int $width
 * @property-read int $height
 * @property-read string $documentBackDeliveryType
 * @property-read int $returnDeliveryPrice
 * @property-read int $declaredPrice
 * @property-read int $declaredPriceSurcharge
 * @property-read array $discounts
 * @property-read string $tariffToken
 * @property-read string $surchargeTariffToken
 * @property-read string $lengthOverpayRatio
 * @property-read string $returnDeliveryPriceRatio
 * @property-read string $returnDeliveryPriceValue
 * @property-read int $measurablesMaxLength
 * @property-read int $measurablesMaxWidth
 * @property-read int $measurablesMaxHeight
 * @property-read int $measurablesTotalWeight
 * @property-read bool $specialTariff
 * @property-read bool $sms
 * @property-read bool $documentBack
 * @property-read bool $withDeliveryNotification
 * */
class DeliveryPrice extends Entity implements BeEntity
{
    /**
     * @return DeliveryPriceAddress
     */
    public function addressFrom(): ?DeliveryPriceAddress
    {
        return empty($this->addressFrom) ? null : new DeliveryPriceAddress($this->addressFrom);
    }

    /**
     * @return DeliveryPriceAddress
     */
    public function addressTo(): ?DeliveryPriceAddress
    {
        return empty($this->addressTo) ? null : new DeliveryPriceAddress($this->addressTo);
    }

    /**
     * @return DeliveryPriceDiscount[]
     */
    public function discounts(): array
    {
        return self::collectFrom('discounts', DeliveryPriceDiscount::class);
    }

    /**
     * @return DeliveryPriceParcel[]
     */
    public function parcels(): array
    {
        return self::collectFrom('parcels', DeliveryPriceParcel::class);
    }
}
