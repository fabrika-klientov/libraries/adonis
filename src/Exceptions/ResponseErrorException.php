<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Adonis
 * @category  Exceptions
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Adonis\Exceptions;

use Adonis\Entities\DataError;

class ResponseErrorException extends HttpClientException
{
    /**
     * @return DataError
     */
    public function getDataError(): ?DataError
    {
        return new DataError($this->dataError ?? []);
    }
}
