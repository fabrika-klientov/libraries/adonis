<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Adonis
 * @category  Core
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Adonis\Core\Http;

use Adonis\Core\Auth\AuthService;
use Adonis\Core\Auth\Credentials;
use Adonis\Exceptions\AuthException;
use Adonis\Exceptions\HttpClientException;
use Adonis\Exceptions\NotFoundException;
use Adonis\Exceptions\ResponseErrorException;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;

class HttpClient
{
    public const USE_API = 1;
    public const USE_FORM = 2;
    public const USE_STATUS_TRACK = 3;
    public const USE_ADDRESS_CLASSIFIER = 4;

    public const API_URL = 'https://www.ukrposhta.ua/ecom/';
    public const API_URL_TEST = 'https://dev.ukrposhta.ua/ecom/';

    public const FORM_URL = 'https://www.ukrposhta.ua/forms/ecom/';
    public const FORM_URL_TEST = 'https://dev.ukrposhta.ua/forms/ecom/';

    public const STATUS_TRACK_URL = 'https://www.ukrposhta.ua/status-tracking/';
    public const STATUS_TRACK_URL_TEST = 'https://www.ukrposhta.ua/status-tracking/';

    public const ADDRESS_CLASSIFIER_URL = 'https://www.ukrposhta.ua/address-classifier-ws/';
    public const ADDRESS_CLASSIFIER_URL_TEST = 'https://www.ukrposhta.ua/address-classifier-ws/';

    public const API_VER = '0.0.1';
    public const API_VER_TEST = '0.0.1';

    protected const TIMEOUT = 30;

    public const GET = 'GET';
    public const POST = 'POST';
    public const PUT = 'PUT';
    public const PATCH = 'PATCH';
    public const DELETE = 'DELETE';

    /**
     * @var AuthService $authService
     * */
    private $authService;

    /**
     * @var int $useType
     * */
    private $useType;

    /**
     * @var bool $useUserToken
     * */
    private $useUserToken;

    /**
     * @param AuthService $authService
     * @param int $use
     */
    public function __construct(AuthService $authService, int $use = self::USE_API, bool $useUserToken = false)
    {
        $this->authService = $authService;
        $this->useType = $use;
        $this->useUserToken = $useUserToken;
    }

    /**
     * @param int $useType
     */
    public function setUseType(int $useType): void
    {
        $this->useType = $useType;
    }

    /**
     * @param bool $useUserToken
     */
    public function setUseUserToken(bool $useUserToken): void
    {
        $this->useUserToken = $useUserToken;
    }

    /** api GET
     * @param string $link
     * @param array $query
     * @param array $options
     * @return array|null
     * @throws HttpClientException
     */
    public function get(string $link, array $query = [], array $options = [])
    {
        return $this->request(
            self::GET,
            $link,
            array_merge($options, ['query' => $query])
        );
    }

    /** api POST
     * @param string $link
     * @param array $data
     * @param array $options
     * @return array|null
     * @throws HttpClientException
     */
    public function post(string $link, array $data, array $options = [])
    {
        return $this->request(
            self::POST,
            $link,
            array_merge($options, empty($data) ? [] : ['json' => $data])
        );
    }

    /** api PUT
     * @param string $link
     * @param array $data
     * @param array $options
     * @return array|null
     * @throws HttpClientException
     */
    public function put(string $link, array $data, array $options = [])
    {
        return $this->request(
            self::PUT,
            $link,
            array_merge($options, empty($data) ? [] : ['json' => $data])
        );
    }

    /** api DELETE
     * @param string $link
     * @param array $query
     * @param array $options
     * @return array|null
     * @throws HttpClientException
     */
    public function delete(string $link, array $query = [], array $options = [])
    {
        return $this->request(
            self::DELETE,
            $link,
            array_merge($options, ['query' => $query])
        );
    }

    /**
     * @param int|null $use
     * @param bool $withAuth
     * @return Client
     * @throws AuthException
     */
    public function getClient(int $use = null, bool $withAuth = true): Client
    {
        $use = $use ?? $this->useType;

        $baseUri = self::getLink('', $use);

        $options = [
            'base_uri' => $baseUri,
            'headers' => [
                'Accept' => 'application/json',
            ],
            'timeout' => self::TIMEOUT,
        ];

        if ($withAuth) {
            $options = self::injectAuth($options, $use);
        }

        return new Client($options);
    }

    /**
     * @param string $method
     * @param string $link
     * @param array $options
     * @return array|string|null
     * @throws HttpClientException
     * */
    public function request(string $method, string $link, array $options, bool $src = false)
    {
        try {
            $response = $this
                ->getClient()
                ->request($method, $this->getLink($link), self::prepareOptions($method, $link, $options));

            $content = $response->getBody()->getContents();
            if ($src) {
                return $content;
            }

            $result = json_decode($content, true);

            return $result;
        } catch (HttpClientException $scarletException) {
            throw new HttpClientException('Request returned error. ' . $scarletException->getMessage());
        } catch (ClientException $clientException) {
            $code = $clientException->getCode();
            switch ($code) {
                case 404:
                    throw new NotFoundException('Data not found. ' . $clientException->getMessage(), $code);
                case 400:
                    $exception = new ResponseErrorException('Response error. ' . $clientException->getMessage(), $code);
                    $exception->setDataError(
                        json_decode($clientException->getResponse()->getBody()->getContents() ?: '[]', true)
                    );

                    throw $exception;
                default:
                    throw new HttpClientException('Request returned error. ' . $clientException->getMessage(), $code);
            }
        } catch (ServerException $exception) {
            throw new HttpClientException('Request returned error. ' . $exception->getMessage());
        } catch (\Throwable $exception) {
            throw new HttpClientException('Request returned error. ' . $exception->getMessage());
        }
    }

    /**
     * @param string $link
     * @param int|null $use
     * @return string
     */
    public function getLink(string $link = '', int $use = null): string
    {
        $use = $use ?? $this->useType;
        $testMode = self::isTestMode();
        switch ($use) {
            case self::USE_FORM:
                $uri = $testMode ? self::FORM_URL_TEST : self::FORM_URL;
                break;

            case self::USE_STATUS_TRACK:
                $uri = $testMode ? self::STATUS_TRACK_URL_TEST : self::STATUS_TRACK_URL;
                break;

            case self::USE_ADDRESS_CLASSIFIER:
                $uri = $testMode ? self::ADDRESS_CLASSIFIER_URL_TEST : self::ADDRESS_CLASSIFIER_URL;
                break;

            default:
                $uri = $testMode ? self::API_URL_TEST : self::API_URL;
        }

        if (in_array($use, [self::USE_FORM, self::USE_STATUS_TRACK, self::USE_API])) {
            $uri .= $testMode ? self::API_VER_TEST : self::API_VER;
        }

        $uri = preg_replace('/\/$/', '', $uri);

        if ($link) {
            $link = preg_replace('/^\//', '', $link);
            $uri .= "/$link";
        }

        return $uri;
    }

    /**
     * @param string $method
     * @param string $link
     * @param array $options
     * @return array
     *
     * @throws AuthException
     */
    protected function prepareOptions(string $method, string $link, array $options): array
    {
        $testMode = self::isTestMode();

        // prepare headers
        $headers = $options['headers'] ?? [];
        switch ($method) {
            case self::POST:
            case self::PUT:
            case self::PATCH:
                if (empty($headers['Content-Type'])) {
                    $headers['Content-Type'] = 'application/json';
                }
                break;
        }
        $options['headers'] = $headers;

        // prepare query (add token)
        if ($this->useUserToken) {
            $query = $options['query'] ?? [];
            $message = 'Token type [%s] is required';
            $credentials = $this->authService->getCredentials();

            if ($testMode) {
                if (!($token = $credentials->getCounterpartyTokenSand())) {
                    throw new AuthException(sprintf($message, Credentials::COUNTERPARTY_TOKEN));
                }
            } else {
                if (!($token = $credentials->getCounterpartyToken())) {
                    throw new AuthException(sprintf($message, Credentials::COUNTERPARTY_TOKEN_SAND));
                }
            }

            $query['token'] = $token;

            $options['query'] = $query;
        }

        return $options;
    }

    /**
     * @param array $options
     * @param int $use
     * @return array
     * @throws AuthException
     */
    protected function injectAuth(array $options, int $use): array
    {
        $headers = $options['headers'] ?? [];
        $message = 'Token type [%s] is required';
        $credentials = $this->authService->getCredentials();
        $testMode = self::isTestMode();

        switch ($use) {
            case self::USE_STATUS_TRACK:
                if ($testMode) {
                    if (!($bearer = $credentials->getStatusTrackingTokenSand())) {
                        throw new AuthException(sprintf($message, Credentials::STATUS_TRACKING_TOKEN_SAND));
                    }
                } else {
                    if (!($bearer = $credentials->getStatusTrackingToken())) {
                        throw new AuthException(sprintf($message, Credentials::STATUS_TRACKING_TOKEN));
                    }
                }
                break;

            case self::USE_FORM:
            case self::USE_API:
            default:
                if ($testMode) {
                    if (!($bearer = $credentials->getEComTokenSand())) {
                        throw new AuthException(sprintf($message, Credentials::E_COM_TOKEN_SAND));
                    }
                } else {
                    if (!($bearer = $credentials->getEComToken())) {
                        throw new AuthException(sprintf($message, Credentials::E_COM_TOKEN));
                    }
                }
        }

        $headers['Authorization'] = sprintf('Bearer %s', $bearer);

        $options['headers'] = $headers;

        return $options;
    }

    /**
     * @return bool
     */
    protected function isTestMode(): bool
    {
        return $this->authService->isTestMode();
    }
}
