<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Adonis
 * @category  Core
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Adonis\Core\Auth;

class Credentials
{
    const E_COM_TOKEN = 'PRODUCTION BEARER eCom';
    const STATUS_TRACKING_TOKEN = 'PRODUCTION BEARER StatusTracking';
    const COUNTERPARTY_TOKEN = 'PROD_COUNTERPARTY TOKEN';
    const E_COM_TOKEN_SAND = 'SANDBOX BEARER eCom';
    const STATUS_TRACKING_TOKEN_SAND = 'SANDBOX BEARER StatusTracking';
    const COUNTERPARTY_TOKEN_SAND = 'SAND_COUNTERPARTY TOKEN';
    const COUNTERPARTY_UUID = 'COUNTERPARTY UUID';

    /**
     * @var string|null $eComToken
     * */
    private $eComToken;
    /**
     * @var string|null $statusTrackingToken
     * */
    private $statusTrackingToken;
    /**
     * @var string|null $counterpartyToken
     * */
    private $counterpartyToken;

    /**
     * @var string|null $eComTokenSand
     * */
    private $eComTokenSand;
    /**
     * @var string|null $statusTrackingTokenSand
     * */
    private $statusTrackingTokenSand;
    /**
     * @var string|null $counterpartyTokenSand
     * */
    private $counterpartyTokenSand;

    /**
     * @var string|null $counterpartyUuid
     * */
    private $counterpartyUuid;

    public function __construct(
        string $eComToken = null,
        string $statusTrackingToken = null,
        string $counterpartyToken = null,
        string $eComTokenSand = null,
        string $statusTrackingTokenSand = null,
        string $counterpartyTokenSand = null,
        string $counterpartyUuid = null
    ) {
        $this->eComToken = $eComToken;
        $this->statusTrackingToken = $statusTrackingToken;
        $this->counterpartyToken = $counterpartyToken;
        $this->eComTokenSand = $eComTokenSand;
        $this->statusTrackingTokenSand = $statusTrackingTokenSand;
        $this->counterpartyTokenSand = $counterpartyTokenSand;
        $this->counterpartyUuid = $counterpartyUuid;
    }

    /**
     * @return string|null
     */
    public function getEComToken(): ?string
    {
        return $this->eComToken;
    }

    /**
     * @param string|null $eComToken
     */
    public function setEComToken(?string $eComToken): void
    {
        $this->eComToken = $eComToken;
    }

    /**
     * @return string|null
     */
    public function getStatusTrackingToken(): ?string
    {
        return $this->statusTrackingToken;
    }

    /**
     * @param string|null $statusTrackingToken
     */
    public function setStatusTrackingToken(?string $statusTrackingToken): void
    {
        $this->statusTrackingToken = $statusTrackingToken;
    }

    /**
     * @return string|null
     */
    public function getCounterpartyToken(): ?string
    {
        return $this->counterpartyToken;
    }

    /**
     * @param string|null $counterpartyToken
     */
    public function setCounterpartyToken(?string $counterpartyToken): void
    {
        $this->counterpartyToken = $counterpartyToken;
    }

    /**
     * @return string|null
     */
    public function getEComTokenSand(): ?string
    {
        return $this->eComTokenSand;
    }

    /**
     * @param string|null $eComTokenSand
     */
    public function setEComTokenSand(?string $eComTokenSand): void
    {
        $this->eComTokenSand = $eComTokenSand;
    }

    /**
     * @return string|null
     */
    public function getStatusTrackingTokenSand(): ?string
    {
        return $this->statusTrackingTokenSand;
    }

    /**
     * @param string|null $statusTrackingTokenSand
     */
    public function setStatusTrackingTokenSand(?string $statusTrackingTokenSand): void
    {
        $this->statusTrackingTokenSand = $statusTrackingTokenSand;
    }

    /**
     * @return string|null
     */
    public function getCounterpartyTokenSand(): ?string
    {
        return $this->counterpartyTokenSand;
    }

    /**
     * @param string|null $counterpartyTokenSand
     */
    public function setCounterpartyTokenSand(?string $counterpartyTokenSand): void
    {
        $this->counterpartyTokenSand = $counterpartyTokenSand;
    }

    /**
     * @return string|null
     */
    public function getCounterpartyUuid(): ?string
    {
        return $this->counterpartyUuid;
    }

    /**
     * @param string|null $counterpartyUuid
     */
    public function setCounterpartyUuid(?string $counterpartyUuid): void
    {
        $this->counterpartyUuid = $counterpartyUuid;
    }
}
