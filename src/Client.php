<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Adonis
 * @category  Adonis
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Adonis;

use Adonis\Core\Auth\AuthService;
use Adonis\Core\Auth\Credentials;
use Adonis\Core\Http\HttpClient;
use Adonis\Services;

class Client
{
    private $authService;
    private $httpClient;

    public function __construct(Credentials $credentials, bool $testMode = false)
    {
        $this->authService = new AuthService($credentials, $testMode);
        $this->httpClient = new HttpClient($this->authService);
    }

    /**
     * @return AuthService
     */
    public function getAuthService(): AuthService
    {
        return $this->authService;
    }

    /**
     * @return HttpClient
     */
    public function getHttpClient(): HttpClient
    {
        return $this->httpClient;
    }

    // additional services

    /**
     * @return Services\AddressClassifierService
     */
    public function getAddressClassifierService(): Services\AddressClassifierService
    {
        return new Services\AddressClassifierService($this);
    }

    /**
     * @return Services\AddressesService
     */
    public function getAddressesService(): Services\AddressesService
    {
        return new Services\AddressesService($this);
    }

    /**
     * @return Services\ClientsService
     */
    public function getClientsService(): Services\ClientsService
    {
        return new Services\ClientsService($this);
    }

    /**
     * @return Services\ShipmentsService
     */
    public function getShipmentsService(): Services\ShipmentsService
    {
        return new Services\ShipmentsService($this);
    }

    /**
     * @return Services\DeliveryNotificationsService
     */
    public function getDeliveryNotificationsService(): Services\DeliveryNotificationsService
    {
        return new Services\DeliveryNotificationsService($this);
    }

    /**
     * @return Services\DocumentBackService
     */
    public function getDocumentBackService(): Services\DocumentBackService
    {
        return new Services\DocumentBackService($this);
    }

    /**
     * @return Services\ShipmentGroupsService
     */
    public function getShipmentGroupsService(): Services\ShipmentGroupsService
    {
        return new Services\ShipmentGroupsService($this);
    }

    /**
     * @return Services\TariffsService
     */
    public function getTariffsService(): Services\TariffsService
    {
        return new Services\TariffsService($this);
    }

    /**
     * @return Services\PaginatorService
     */
    public function getPaginatorService(): Services\PaginatorService
    {
        return new Services\PaginatorService($this);
    }

    /**
     * @return Services\FormsService
     */
    public function getFormsService(): Services\FormsService
    {
        return new Services\FormsService($this);
    }

    /**
     * @return Services\StatusTrackingService
     */
    public function getStatusTrackingService(): Services\StatusTrackingService
    {
        return new Services\StatusTrackingService($this);
    }

    /**
     * @return Services\LettersService
     */
    public function getLettersService(): Services\LettersService
    {
        return new Services\LettersService($this);
    }
}
