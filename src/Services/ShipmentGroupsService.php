<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Adonis
 * @category  Services
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Adonis\Services;

use Adonis\Core\Http\HttpClient;
use Adonis\Entities\DeliveryNotifications\ShipmentWithDeliveryNotification;
use Adonis\Entities\DocumentBack\ShipmentWithDocumentBack;
use Adonis\Entities\DocumentBack\StickerDocumentBack;
use Adonis\Entities\DocumentBack\StickerShipmentWithStickerDocumentBack;
use Adonis\Entities\ShipmentGroups as Entities;
use Adonis\Entities\ShipmentGroups\Requests;
use Adonis\Entities\Shipments\Requests\StoreShipmentRequest;
use Adonis\Entities\Shipments\Shipment;
use Illuminate\Support\Collection;

class ShipmentGroupsService extends BaseService
{
    protected const SHIPMENT_GROUPS = 'shipment-groups';
    protected const SHIPMENT_GROUPS_SHIPMENTS = 'shipment-groups/%s/shipments';
    protected const SHIPMENT_GROUPS_CLIENTS = 'shipment-groups/clients';
    protected const SHIPMENTS_SHIPMENT_GROUP = 'shipments/shipment-group';
    protected const CLIENTS_SHIPMENT_GROUPS_DATE = 'clients/%s/shipment-groups/from/%s/to/%s';
    protected const CLIENTS_SHIPMENT_GROUPS_NAME = 'clients/%s/shipment-groups/name';
    protected const CLIENTS_SHIPMENT_GROUPS_DATE_AND_NAME = 'clients/%s/shipment-groups/name/%s/from/%s/to/%s';
    protected const SHIPMENTS_SHIPMENT_GROUP_DETAIL = 'shipments/%s/shipment-group';
    protected const SHIPMENT_GROUPS_SHIPMENTS_WITH_DELIVERY_NOTIFICATIONS = 'shipment-groups/%s/shipments-with-delivery-notifications';
    protected const SHIPMENT_GROUPS_SHIPMENTS_WITH_DELIVERY_NOTIFICATIONS_BARCODE = 'shipment-groups/shipments/%s/shipments-with-delivery-notifications';
    protected const SHIPMENT_GROUPS_SHIPMENTS_AND_DELIVERY_NOTIFICATIONS = 'shipment-groups/%s/shipments-and-delivery-notifications';
    protected const SHIPMENT_GROUPS_SHIPMENTS_AND_DELIVERY_NOTIFICATIONS_BARCODE = 'shipment-groups/shipments/%s/shipments-and-delivery-notifications';
    protected const DOCUMENT_BACK_WITH_SHIPMENT = 'document-back-with/shipments/by/shipment-group';
    protected const SHIPMENT_GROUPS_DOCUMENT_BACK_WITH_SHIPMENT = 'shipment-groups/%s/shipments-with-document-back';
    protected const SHIPMENT_GROUPS_DOCUMENT_BACK_WITH_SHIPMENT_BARCODE = 'shipment-groups/shipments/%s/shipments-with-document-back';
    protected const SHIPMENT_GROUPS_DOCUMENT_BACK_AND_SHIPMENT = 'shipment-groups/%s/shipments-and-document-back';
    protected const SHIPMENT_GROUPS_DOCUMENT_BACK_AND_SHIPMENT_BARCODE = 'shipment-groups/shipments/%s/shipments-and-document-back';
    protected const SHIPMENT_GROUPS_DOCUMENT_BACK_STICKER = 'shipment-groups/%s/document-back/sticker-json';
    protected const SHIPMENT_GROUPS_DOCUMENT_BACK_WITH_SHIPMENT_STICKER = 'shipment-groups/%s/document-back-with-shipment/sticker-json';

    protected $useUserToken = true;

    public function store(Requests\StoreShipmentGroupRequest $request): ?Entities\ShipmentGroup
    {
        return self::factoryEntity(
            self::doRequest(HttpClient::POST, self::SHIPMENT_GROUPS, $request),
            Entities\ShipmentGroup::class
        );
    }

    public function storeList(Collection $list): Collection
    {
        return self::factoryEntitiesCollect(
            self::doRequest(HttpClient::POST, self::SHIPMENT_GROUPS, $list),
            Entities\ShipmentGroup::class
        );
    }

    public function addShipmentToGroup(string $groupUuid, string $shipmentUuid): ?Entities\Message
    {
        return self::factoryEntity(
            self::doRequest(
                HttpClient::POST,
                sprintf(self::SHIPMENT_GROUPS_SHIPMENTS, $groupUuid) . "/$shipmentUuid",
                null
            ),
            Entities\Message::class
        );
    }

    /**
     * @param string $groupUuid
     * @param string[] $shipmentList
     */
    public function addShipmentsToGroup(string $groupUuid, array $shipmentList): Collection
    {
        return self::factoryEntitiesCollect(
            self::doRequest(HttpClient::PUT, sprintf(self::SHIPMENT_GROUPS_SHIPMENTS, $groupUuid), $shipmentList),
            Entities\ShipmentGroupUpdatedMessage::class
        );
    }

    public function storeShipmentInGroup(string $groupUuid, StoreShipmentRequest $request): ?Shipment
    {
        return self::factoryEntity(
            self::doRequest(HttpClient::POST, sprintf(self::SHIPMENT_GROUPS_SHIPMENTS, $groupUuid), $request),
            Shipment::class
        );
    }

    public function storeShipmentsInGroup(string $groupUuid, Collection $list): Collection
    {
        return self::factoryEntitiesCollect(
            self::doRequest(HttpClient::POST, sprintf(self::SHIPMENT_GROUPS_SHIPMENTS, $groupUuid), $list),
            Shipment::class
        );
    }

    public function update(string $groupUuid, Requests\UpdateShipmentGroupRequest $request): ?Entities\ShipmentGroup
    {
        return self::factoryEntity(
            self::doRequest(HttpClient::PUT, self::SHIPMENT_GROUPS . "/$groupUuid", $request),
            Entities\ShipmentGroup::class
        );
    }

    public function findGroup(string $groupUuid): ?Entities\ShipmentGroup
    {
        return self::factoryEntity(
            self::doRequest(HttpClient::GET, self::SHIPMENT_GROUPS . "/$groupUuid", null),
            Entities\ShipmentGroup::class
        );
    }

    public function findGroupsByClient(string $clientUuid): Collection
    {
        $result = self::doRequest(HttpClient::GET, self::SHIPMENT_GROUPS_CLIENTS . "/$clientUuid", null);
        if (isset($result['uuid'])) {
            $result = [$result];
        }

        return self::factoryEntitiesCollect($result, Entities\ShipmentGroup::class);
    }

    public function findGroupByShipment(string $shipmentUuid): ?Entities\ShipmentGroup
    {
        return self::factoryEntity(
            self::doRequest(HttpClient::GET, self::SHIPMENTS_SHIPMENT_GROUP . "/$shipmentUuid", null),
            Entities\ShipmentGroup::class
        );
    }

    public function findGroupsByClientAndDates(string $clientUuid, string $from, string $to): Collection
    {
        $result = self::doRequest(
            HttpClient::GET,
            sprintf(self::CLIENTS_SHIPMENT_GROUPS_DATE, $clientUuid, $from, $to),
            null
        );
        if (isset($result['uuid'])) {
            $result = [$result];
        }

        return self::factoryEntitiesCollect($result, Entities\ShipmentGroup::class);
    }

    public function findGroupsByClientAndName(string $clientUuid, string $name): Collection
    {
        $result = self::doRequest(
            HttpClient::GET,
            sprintf(self::CLIENTS_SHIPMENT_GROUPS_NAME, $clientUuid) . "/$name",
            null
        );
        if (isset($result['uuid'])) {
            $result = [$result];
        }

        return self::factoryEntitiesCollect($result, Entities\ShipmentGroup::class);
    }

    public function findGroupsByClientAndNameWithDates(
        string $clientUuid,
        string $name,
        string $from,
        string $to
    ): Collection {
        $result = self::doRequest(
            HttpClient::GET,
            sprintf(self::CLIENTS_SHIPMENT_GROUPS_DATE_AND_NAME, $clientUuid, $name, $from, $to),
            null
        );
        if (isset($result['uuid'])) {
            $result = [$result];
        }

        return self::factoryEntitiesCollect($result, Entities\ShipmentGroup::class);
    }

    public function findGroupsByClientAndType(string $clientUuid, string $type): Collection
    {
        $result = self::doRequest(HttpClient::GET, self::SHIPMENT_GROUPS_CLIENTS . "/$clientUuid", ['type' => $type]);
        if (isset($result['uuid'])) {
            $result = [$result];
        }

        return self::factoryEntitiesCollect($result, Entities\ShipmentGroup::class);
    }

    public function detachShipmentFromGroup(string $shipmentUuid): ?Shipment
    {
        return self::factoryEntity(
            self::doRequest(HttpClient::DELETE, sprintf(self::SHIPMENTS_SHIPMENT_GROUP_DETAIL, $shipmentUuid), null),
            Shipment::class
        );
    }

    public function getShipmentsWithDeliveryNotificationsForGroup(string $groupUuid): Collection
    {
        return self::factoryEntitiesCollect(
            self::doRequest(
                HttpClient::GET,
                sprintf(self::SHIPMENT_GROUPS_SHIPMENTS_WITH_DELIVERY_NOTIFICATIONS, $groupUuid),
                null
            ),
            ShipmentWithDeliveryNotification::class
        );
    }

    public function getShipmentsWithDeliveryNotificationsForGroupByBarcode(string $barcode): Collection
    {
        return self::factoryEntitiesCollect(
            self::doRequest(
                HttpClient::GET,
                sprintf(self::SHIPMENT_GROUPS_SHIPMENTS_WITH_DELIVERY_NOTIFICATIONS_BARCODE, $barcode),
                null
            ),
            ShipmentWithDeliveryNotification::class
        );
    }

    public function getShipmentsAndDeliveryNotificationsForGroup(string $groupUuid): Collection
    {
        return self::factoryEntitiesCollect(
            self::doRequest(
                HttpClient::GET,
                sprintf(self::SHIPMENT_GROUPS_SHIPMENTS_AND_DELIVERY_NOTIFICATIONS, $groupUuid),
                null
            ),
            ShipmentWithDeliveryNotification::class
        )
            ->values();
    }

    public function getShipmentsAndDeliveryNotificationsForGroupByBarcode(string $barcode): Collection
    {
        return self::factoryEntitiesCollect(
            self::doRequest(
                HttpClient::GET,
                sprintf(self::SHIPMENT_GROUPS_SHIPMENTS_AND_DELIVERY_NOTIFICATIONS_BARCODE, $barcode),
                null
            ),
            ShipmentWithDeliveryNotification::class
        );
    }

    public function getShipmentsForGroup(string $groupUuid, $status = null): Collection
    {
        $status = empty($status) ? null : (is_array($status) ? join(',', $status) : $status);

        return self::factoryEntitiesCollect(
            self::doRequest(
                HttpClient::GET,
                sprintf(self::SHIPMENT_GROUPS_SHIPMENTS, $groupUuid),
                empty($status) ? null : ['status' => $status]
            ),
            ShipmentWithDeliveryNotification::class
        );
    }

    public function getShipmentsWithDocumentBacksForGroup(string $groupUuid): Collection
    {
        return self::factoryEntitiesCollect(
            self::doRequest(
                HttpClient::GET,
                self::DOCUMENT_BACK_WITH_SHIPMENT . "/$groupUuid",
                null
            ),
            ShipmentWithDocumentBack::class
        );
    }

    public function getShipmentsWithDocumentBacksForGroup2(string $groupUuid): Collection
    {
        return self::factoryEntitiesCollect(
            self::doRequest(
                HttpClient::GET,
                sprintf(self::SHIPMENT_GROUPS_DOCUMENT_BACK_WITH_SHIPMENT, $groupUuid),
                null
            ),
            ShipmentWithDocumentBack::class
        );
    }

    public function getShipmentsWithDocumentBacksForGroupByBarcode(string $barcode): Collection
    {
        return self::factoryEntitiesCollect(
            self::doRequest(
                HttpClient::GET,
                sprintf(self::SHIPMENT_GROUPS_DOCUMENT_BACK_WITH_SHIPMENT_BARCODE, $barcode),
                null
            ),
            ShipmentWithDocumentBack::class
        );
    }

    public function getShipmentsAndDocumentBacksForGroup2(string $groupUuid): Collection
    {
        return self::factoryEntitiesCollect(
            self::doRequest(
                HttpClient::GET,
                sprintf(self::SHIPMENT_GROUPS_DOCUMENT_BACK_AND_SHIPMENT, $groupUuid),
                null
            ),
            ShipmentWithDocumentBack::class
        )
            ->values();
    }

    public function getShipmentsAndDocumentBacksForGroupByBarcode(string $barcode): Collection
    {
        return self::factoryEntitiesCollect(
            self::doRequest(
                HttpClient::GET,
                sprintf(self::SHIPMENT_GROUPS_DOCUMENT_BACK_AND_SHIPMENT_BARCODE, $barcode),
                null
            ),
            ShipmentWithDocumentBack::class
        )
            ->values();
    }

    public function getStickerDocumentBackByGroup(string $groupUuid): Collection
    {
        return self::factoryEntitiesCollect(
            self::doRequest(
                HttpClient::GET,
                sprintf(self::SHIPMENT_GROUPS_DOCUMENT_BACK_STICKER, $groupUuid),
                null
            ),
            StickerDocumentBack::class
        )
            ->values();
    }

    public function getStickerDocumentBackWithShipmentByGroup(string $groupUuid): Collection
    {
        return self::factoryEntitiesCollect(
            self::doRequest(
                HttpClient::GET,
                sprintf(self::SHIPMENT_GROUPS_DOCUMENT_BACK_WITH_SHIPMENT_STICKER, $groupUuid),
                null
            ),
            StickerShipmentWithStickerDocumentBack::class
        )
            ->values();
    }

    protected function useType(): int
    {
        return HttpClient::USE_API;
    }
}
