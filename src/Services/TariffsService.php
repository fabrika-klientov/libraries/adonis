<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Adonis
 * @category  Services
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Adonis\Services;

use Adonis\Core\Http\HttpClient;
use Adonis\Entities\Tariffs as Entities;
use Adonis\Entities\Tariffs\Requests;
use Illuminate\Support\Collection;

class TariffsService extends BaseService
{
    protected const TARIFFS = 'dictionaries/tariffs';
    protected const DELIVERY_PRICE = 'domestic/delivery-price';

    public function list(): Collection
    {
        return self::factoryEntitiesCollect(
            self::doRequest(HttpClient::GET, self::TARIFFS, null),
            Entities\Tariff::class
        );
    }

    public function find(string $uuid): ?Entities\Tariff
    {
        return self::factoryEntity(
            self::doRequest(HttpClient::GET, self::TARIFFS . "/$uuid", null),
            Entities\Tariff::class
        );
    }

    public function deliveryPrice(Requests\DeliveryPriceRequest $request): ?Entities\DeliveryPrice
    {
        return self::factoryEntity(
            self::doRequest(HttpClient::POST, self::DELIVERY_PRICE, $request),
            Entities\DeliveryPrice::class
        );
    }

    protected function useType(): int
    {
        return HttpClient::USE_API;
    }
}
