<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Adonis
 * @category  Services
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Adonis\Services;

use Adonis\Core\Http\HttpClient;
use Adonis\Entities\StatusTracking as Entities;
use Illuminate\Support\Collection;

class StatusTrackingService extends BaseService
{
    protected const STATUSES = 'statuses';
    protected const STATUSES_LAST = 'statuses/last';
    protected const STATUSES_WITH_NOT_FOUND = 'statuses/with-not-found';
    protected const STATUSES_WITH_NOT_FOUND_LAST = 'statuses/last/with-not-found';
    protected const ROUTE = 'barcodes/%s/route';
    protected const ROUTE_LANG = 'barcodes/%s/route/in-lang/%s';

    public function statuses($barcode): Collection
    {
        return self::factoryEntitiesCollect(
            self::doRequest(HttpClient::GET, self::STATUSES, ['barcode' => $barcode]),
            Entities\TrackingStatus::class
        );
    }

    public function lastStatus($barcode): ?Entities\TrackingStatus
    {
        return self::factoryEntity(
            self::doRequest(HttpClient::GET, self::STATUSES_LAST, ['barcode' => $barcode]),
            Entities\TrackingStatus::class
        );
    }

    public function statusesForList(array $barcodes): Collection
    {
        return self::factoryEntitiesCollect(
            self::doRequest(HttpClient::POST, self::STATUSES, $barcodes),
            Entities\TrackingStatus::class
        );
    }

    public function statusesForListWithCheck(array $barcodes): Entities\TrackingCheck
    {
        return self::factoryEntity(
            self::doRequest(HttpClient::POST, self::STATUSES_WITH_NOT_FOUND, $barcodes),
            Entities\TrackingCheck::class
        );
    }

    public function lastStatusForList(array $barcodes): Collection
    {
        return self::factoryEntitiesCollect(
            self::doRequest(HttpClient::POST, self::STATUSES_LAST, $barcodes),
            Entities\TrackingStatus::class
        );
    }

    public function lastStatusForListWithCheck(array $barcodes): Entities\TrackingCheck
    {
        return self::factoryEntity(
            self::doRequest(HttpClient::POST, self::STATUSES_WITH_NOT_FOUND_LAST, $barcodes),
            Entities\TrackingCheck::class
        );
    }

    public function route($barcode, string $lang = null): Entities\TrackingRoute
    {
        return self::factoryEntity(
            self::doRequest(
                HttpClient::GET,
                empty($lang) ? sprintf(self::ROUTE, $barcode) : sprintf(self::ROUTE_LANG, $barcode, $lang),
                null
            ),
            Entities\TrackingRoute::class
        );
    }

    protected function useType(): int
    {
        return HttpClient::USE_STATUS_TRACK;
    }
}
