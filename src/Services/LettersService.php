<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Adonis
 * @category  Services
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Adonis\Services;

use Adonis\Core\Http\HttpClient;
use Adonis\Entities\Letters as Entities;
use Adonis\Entities\Letters\Requests;
use Illuminate\Support\Collection;

class LettersService extends BaseService
{
    protected const LETTERS_DOMESTIC = 'letters/domestic';

    protected $useUserToken = true;

    public function store(Requests\StoreLetterRequest $request): ?Entities\Letter
    {
        return self::factoryEntity(
            self::doRequest(HttpClient::POST, self::LETTERS_DOMESTIC, $request),
            Entities\Letter::class
        );
    }

    // TODO: add another methods

    protected function useType(): int
    {
        return HttpClient::USE_API;
    }
}
