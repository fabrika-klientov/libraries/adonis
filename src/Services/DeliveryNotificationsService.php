<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Adonis
 * @category  Services
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Adonis\Services;

use Adonis\Core\Http\HttpClient;
use Adonis\Entities\DeliveryNotifications as Entities;
use Adonis\Entities\Shipments\Shipment;
use Illuminate\Support\Collection;

class DeliveryNotificationsService extends BaseService
{
    protected const SHIPMENTS_DELIVERY_NOTIFICATION = 'shipments/%s/delivery-notification';
    protected const SHIPMENTS_DELIVERY_NOTIFICATIONS = 'shipments/delivery-notification';
    protected const DELIVERY_NOTIFICATIONS_SHIPMENT = 'delivery-notifications/%s/shipment';
    protected const DELIVERY_NOTIFICATIONS_SHIPMENT_PARENT = 'delivery-notifications/%s/parent';
    protected const DELIVERY_NOTIFICATIONS_WITH_SHIPMENT_BY_SG = 'delivery-notifications-with/shipments/by/shipment-group';
    protected const DELIVERY_NOTIFICATIONS_WITH_SHIPMENT_BY_DN = 'delivery-notifications-with/shipments/by/delivery-notifications';
    protected const DELIVERY_NOTIFICATIONS_WITH_SHIPMENT_BY_S = 'delivery-notifications-with/shipments/by/shipment';
    protected const SHIPMENTS_FORM119 = 'shipments/%s/form119/json';
    protected const SHIPMENTS_FORM119_DELIVERY_NOTIFICATION = 'shipments/delivery-notifications/%s/form119/json';
    protected const SHIPMENTS_FORM119_SHIPMENT_GROUPS = 'shipment-groups/%s/form119/json';
    protected const SHIPMENTS_FORM119_WITH_STICKER_SHIPMENT_GROUPS = 'shipment-groups/%s/sticker-form119/json';

    protected $useUserToken = true;

    // delivery notifications & shipments

    public function findDeliveryNotificationByShipment($uuidOrBarcode): ?Entities\ShipmentDeliveryNotification
    {
        return self::factoryEntity(
            self::doRequest(HttpClient::GET, sprintf(self::SHIPMENTS_DELIVERY_NOTIFICATION, $uuidOrBarcode), null),
            Entities\ShipmentDeliveryNotification::class
        );
    }

    public function listDeliveryNotifications(array $uuidsOrBarcodes): Collection
    {
        return self::factoryEntitiesCollect(
            self::doRequest(HttpClient::POST, self::SHIPMENTS_DELIVERY_NOTIFICATIONS, $uuidsOrBarcodes),
            Entities\ShipmentDeliveryNotificationSimple::class
        );
    }

    public function findShipmentByDeliveryNotification($uuidOrBarcode): ?Shipment
    {
        return self::factoryEntity(
            self::doRequest(HttpClient::GET, sprintf(self::DELIVERY_NOTIFICATIONS_SHIPMENT, $uuidOrBarcode), null),
            Shipment::class
        );
    }

    public function findShipmentAsParentByDeliveryNotification($uuidOrBarcode): ?Entities\ShipmentParent
    {
        return self::factoryEntity(
            self::doRequest(
                HttpClient::GET,
                sprintf(self::DELIVERY_NOTIFICATIONS_SHIPMENT_PARENT, $uuidOrBarcode),
                null
            ),
            Entities\ShipmentParent::class
        );
    }

    public function listShipmentsWithDeliveryNotificationsByShipmentGroup(string $uuid): Collection
    {
        return self::factoryEntitiesCollect(
            self::doRequest(
                HttpClient::GET,
                self::DELIVERY_NOTIFICATIONS_WITH_SHIPMENT_BY_SG . "/$uuid",
                null
            ),
            Entities\ShipmentWithDeliveryNotification::class
        );
    }

    public function findShipmentWithDeliveryNotificationByDeliveryNotification(
        $uuidOrBarcode
    ): ?Entities\ShipmentWithDeliveryNotification {
        return self::factoryEntity(
            self::doRequest(
                HttpClient::GET,
                self::DELIVERY_NOTIFICATIONS_WITH_SHIPMENT_BY_DN . "/$uuidOrBarcode",
                null
            ),
            Entities\ShipmentWithDeliveryNotification::class
        );
    }

    public function findShipmentWithDeliveryNotificationByShipment(
        $uuidOrBarcode
    ): ?Entities\ShipmentWithDeliveryNotification {
        return self::factoryEntity(
            self::doRequest(
                HttpClient::GET,
                self::DELIVERY_NOTIFICATIONS_WITH_SHIPMENT_BY_S . "/$uuidOrBarcode",
                null
            ),
            Entities\ShipmentWithDeliveryNotification::class
        );
    }

    // print form 119

    public function printForm119ByShipment($uuidOrBarcode): ?Entities\DeliveryNotificationForm119
    {
        return self::factoryEntity(
            self::doRequest(HttpClient::GET, sprintf(self::SHIPMENTS_FORM119, $uuidOrBarcode), null),
            Entities\DeliveryNotificationForm119::class
        );
    }

    public function printForm119ByDeliveryNotification($uuidOrBarcode): ?Entities\DeliveryNotificationForm119
    {
        return self::factoryEntity(
            self::doRequest(
                HttpClient::GET,
                sprintf(self::SHIPMENTS_FORM119_DELIVERY_NOTIFICATION, $uuidOrBarcode),
                null
            ),
            Entities\DeliveryNotificationForm119::class
        );
    }

    public function printListForm119ByShipmentGroup($uuid): Collection
    {
        return self::factoryEntitiesCollect(
            self::doRequest(
                HttpClient::GET,
                sprintf(self::SHIPMENTS_FORM119_SHIPMENT_GROUPS, $uuid),
                null
            ),
            Entities\DeliveryNotificationForm119::class
        );
    }

    public function printListStickerWithForm119ByShipmentGroup($uuid): Collection
    {
        return self::factoryEntitiesCollect(
            self::doRequest(
                HttpClient::GET,
                sprintf(self::SHIPMENTS_FORM119_WITH_STICKER_SHIPMENT_GROUPS, $uuid),
                null
            ),
            Entities\StickerShipmentWithDeliveryNotificationForm119::class
        );
    }

    protected function useType(): int
    {
        return HttpClient::USE_API;
    }
}
