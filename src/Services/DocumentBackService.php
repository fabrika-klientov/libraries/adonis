<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Adonis
 * @category  Services
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Adonis\Services;

use Adonis\Core\Http\HttpClient;
use Adonis\Entities\DocumentBack as Entities;
use Adonis\Entities\DocumentBack\Requests;
use Adonis\Entities\Shipments\Shipment;

class DocumentBackService extends BaseService
{
    protected const SHIPMENTS_DOCUMENT_BACK = 'shipments/%s/document-back';
    protected const DOCUMENT_BACK = 'document-back';
    protected const DOCUMENT_BACK_SHIPMENTS = 'document-back/%s/shipment';
    protected const DOCUMENT_BACK_WITH_SHIPMENT_BY_DB = 'document-back-with/shipments/by/document-back';
    protected const DOCUMENT_BACK_WITH_SHIPMENT_BY_S = 'document-back-with/shipments/by/shipment';
    protected const STICKER_DOCUMENT_BACK_WITH_STICKER_SHIPMENT_BY_S = 'document-back-with/shipment/by/shipment/%s/sticker-json';
    protected const STICKER_DOCUMENT_BACK_WITH_STICKER_SHIPMENT_BY_DB = 'document-back-with/shipment/%s/sticker-json';
    protected const STICKER_DOCUMENT_BACK_BY_S = 'document-back/by/shipment/%s/sticker-json';
    protected const STICKER_DOCUMENT_BACK = 'document-back/%s/sticker-json';
    protected const DOCUMENT_BACK_RECIPIENT_ADDRESS = 'document-back/%s/recipient-address';
    protected const DOCUMENT_BACK_DELIVERY_TYPE = 'document-back/%s/delivery-type';
    protected const DOCUMENT_BACK_LENGTH = 'document-back/%s/length';
    protected const DOCUMENT_BACK_WEIGHT = 'document-back/%s/weight';

    protected $useUserToken = true;

    // shipment document back & stickers

    public function store(
        $uuidOrBarcode,
        Requests\StoreShipmentDocumentBackRequest $request
    ): ?Entities\ShipmentDocumentBack {
        return self::factoryEntity(
            self::doRequest(HttpClient::PUT, sprintf(self::SHIPMENTS_DOCUMENT_BACK, $uuidOrBarcode), $request),
            Entities\ShipmentDocumentBack::class
        );
    }

    public function findDocumentBack($uuidOrBarcode): ?Entities\ShipmentDocumentBack
    {
        return self::factoryEntity(
            self::doRequest(HttpClient::GET, self::DOCUMENT_BACK . "/$uuidOrBarcode", null),
            Entities\ShipmentDocumentBack::class
        );
    }

    public function findDocumentBackByShipment($uuidOrBarcode): ?Entities\ShipmentDocumentBack
    {
        return self::factoryEntity(
            self::doRequest(HttpClient::GET, sprintf(self::SHIPMENTS_DOCUMENT_BACK, $uuidOrBarcode), null),
            Entities\ShipmentDocumentBack::class
        );
    }

    public function findShipmentByDocumentBack($uuidOrBarcode): ?Shipment
    {
        return self::factoryEntity(
            self::doRequest(HttpClient::GET, sprintf(self::DOCUMENT_BACK_SHIPMENTS, $uuidOrBarcode), null),
            Shipment::class
        );
    }

    public function findShipmentWithDocumentBackByDocumentBack($uuidOrBarcode): ?Entities\ShipmentWithDocumentBack
    {
        return self::factoryEntity(
            self::doRequest(HttpClient::GET, self::DOCUMENT_BACK_WITH_SHIPMENT_BY_DB . "/$uuidOrBarcode", null),
            Entities\ShipmentWithDocumentBack::class
        );
    }

    public function findShipmentWithDocumentBackByShipment($uuidOrBarcode): ?Entities\ShipmentWithDocumentBack
    {
        return self::factoryEntity(
            self::doRequest(HttpClient::GET, self::DOCUMENT_BACK_WITH_SHIPMENT_BY_S . "/$uuidOrBarcode", null),
            Entities\ShipmentWithDocumentBack::class
        );
    }

    public function findStickerShipmentWithStickerDocumentBackByShipment(
        $uuidOrBarcode
    ): ?Entities\StickerShipmentWithStickerDocumentBack {
        return self::factoryEntity(
            self::doRequest(
                HttpClient::GET,
                sprintf(self::STICKER_DOCUMENT_BACK_WITH_STICKER_SHIPMENT_BY_S, $uuidOrBarcode),
                null
            ),
            Entities\StickerShipmentWithStickerDocumentBack::class
        );
    }

    public function findStickerShipmentWithStickerDocumentBackByDocumentBack(
        $uuidOrBarcode
    ): ?Entities\StickerShipmentWithStickerDocumentBack {
        return self::factoryEntity(
            self::doRequest(
                HttpClient::GET,
                sprintf(self::STICKER_DOCUMENT_BACK_WITH_STICKER_SHIPMENT_BY_DB, $uuidOrBarcode),
                null
            ),
            Entities\StickerShipmentWithStickerDocumentBack::class
        );
    }

    public function findStickerDocumentBackByShipment($uuidOrBarcode): ?Entities\StickerDocumentBack
    {
        return self::factoryEntity(
            self::doRequest(HttpClient::GET, sprintf(self::STICKER_DOCUMENT_BACK_BY_S, $uuidOrBarcode), null),
            Entities\StickerDocumentBack::class
        );
    }

    public function findStickerDocumentBack($uuidOrBarcode): ?Entities\StickerDocumentBack
    {
        return self::factoryEntity(
            self::doRequest(HttpClient::GET, sprintf(self::STICKER_DOCUMENT_BACK, $uuidOrBarcode), null),
            Entities\StickerDocumentBack::class
        );
    }

    public function updateDocumentBackRecipientAddress(
        $uuidOrBarcode,
        $recipientAddressId
    ): ?Entities\ShipmentDocumentBack {
        return self::factoryEntity(
            self::doRequest(
                HttpClient::PUT,
                sprintf(self::DOCUMENT_BACK_RECIPIENT_ADDRESS, $uuidOrBarcode) . "/$recipientAddressId",
                null
            ),
            Entities\ShipmentDocumentBack::class
        );
    }

    public function updateDocumentBackDeliveryType($uuidOrBarcode, string $deliveryType): ?Entities\ShipmentDocumentBack
    {
        return self::factoryEntity(
            self::doRequest(
                HttpClient::PUT,
                sprintf(self::DOCUMENT_BACK_DELIVERY_TYPE, $uuidOrBarcode) . "/$deliveryType",
                null
            ),
            Entities\ShipmentDocumentBack::class
        );
    }

    public function updateDocumentBackLength($uuidOrBarcode, $length): ?Entities\ShipmentDocumentBack
    {
        return self::factoryEntity(
            self::doRequest(
                HttpClient::PUT,
                sprintf(self::DOCUMENT_BACK_LENGTH, $uuidOrBarcode) . "/$length",
                null
            ),
            Entities\ShipmentDocumentBack::class
        );
    }

    public function updateDocumentBackWeight($uuidOrBarcode, $weight): ?Entities\ShipmentDocumentBack
    {
        return self::factoryEntity(
            self::doRequest(
                HttpClient::PUT,
                sprintf(self::DOCUMENT_BACK_WEIGHT, $uuidOrBarcode) . "/$weight",
                null
            ),
            Entities\ShipmentDocumentBack::class
        );
    }

    public function deleteDocumentBack($uuidOrBarcode): ?Entities\Message
    {
        return self::factoryEntity(
            self::doRequest(HttpClient::DELETE, sprintf(self::SHIPMENTS_DOCUMENT_BACK, $uuidOrBarcode), null),
            Entities\Message::class
        );
    }

    protected function useType(): int
    {
        return HttpClient::USE_API;
    }
}
