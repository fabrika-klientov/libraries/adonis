<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Adonis
 * @category  Services
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Adonis\Services;

use Adonis\Client;
use Adonis\Contracts\BeRequestEntity;
use Adonis\Core\Http\HttpClient;
use Adonis\Exceptions\HttpClientException;
use Adonis\Exceptions\ValidateRequestException;
use Illuminate\Support\Collection;

abstract class BaseService
{
    protected $client;
    protected $useUserToken = false;
    protected $keysCollect = [];
    protected $keysEntity = [];

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * @return Client
     */
    public function getClient(): Client
    {
        return $this->client;
    }

    /**
     * Prepared HttpClient
     *
     * @return HttpClient
     */
    protected function getHttpClient(): HttpClient
    {
        $client = $this->client->getHttpClient();
        $client->setUseType($this->useType());
        $client->setUseUserToken($this->useUserToken);

        return $client;
    }

    /**
     * @param string $method
     * @param string $link
     * @param BeRequestEntity|Collection|array|null $request
     * @return array|null
     *
     * @throws HttpClientException
     * @throws ValidateRequestException
     */
    protected function doRequest(string $method, string $link, $request)
    {
        try {
            switch (true) {
                case is_null($request):
                    $data = [];
                    break;

                case is_array($request):
                    $data = $request;
                    break;

                case $request instanceof BeRequestEntity:
                    $data = self::_getSingleData($request);
                    break;

                case $request instanceof Collection:
                    if (!$request->every(function ($item) {
                        return $item instanceof BeRequestEntity;
                    })) {
                        throw new ValidateRequestException(
                            "All items of list should be instance of BeRequestEntity [$link]"
                        );
                    }

                    $data = $request
                        ->map(function (BeRequestEntity $item) {
                            return self::_getSingleData($item);
                        })
                        ->values()
                        ->all();
                    break;

                default:
                    throw new ValidateRequestException("Unexpected type Request [$link]");
            }
        } catch (ValidateRequestException $e) {
            $e->setMessage(sprintf($e->getMessage(), $link));

            throw $e;
        }

        $http = $this->getHttpClient();
        switch ($method) {
            case HttpClient::GET:
                return $http->get($link, $data);
            case HttpClient::POST:
                return $http->post($link, $data);
            case HttpClient::PUT:
                return $http->put($link, $data);
            case HttpClient::DELETE:
                return $http->delete($link, $data);
            default:
                throw new HttpClientException("Method [$method] is not resolved");
        }
    }

    /**
     * @param BeRequestEntity $request
     * @return array
     *
     * @throws ValidateRequestException
     */
    private function _getSingleData(BeRequestEntity $request): array
    {
        if (!$request->validate()) {
            throw new ValidateRequestException("Some params are required for this method [%s]");
        }

        return $request->getData();
    }

    /**
     * @param array $result
     * @param string $classEntity
     * @param array|null $keysCollect
     * @param array|null $keysEntity
     * @return Collection
     */
    protected function factoryEntitiesCollect(
        array $result,
        string $classEntity,
        array $keysCollect = null,
        array $keysEntity = null
    ): Collection {
        if (empty($result)) {
            return new Collection();
        }

        $keysCollect = $keysCollect ?? $this->keysCollect;
        foreach ($keysCollect as $key) {
            $result = $result[$key] ?? [];
        }

        return (new Collection($result))
            ->map(function ($item) use ($classEntity, $keysEntity) {
                return $this->factoryEntity($item, $classEntity, $keysEntity);
            });
    }

    /**
     * @param array $result
     * @param string $classEntity
     * @param array|null $keysEntity
     * @return mixed|null
     */
    protected function factoryEntity(array $result, string $classEntity, array $keysEntity = null)
    {
        if (empty($result)) {
            return null;
        }

        $keysEntity = $keysEntity ?? $this->keysEntity;
        foreach ($keysEntity as $key) {
            $result = $result[$key] ?? [];
        }

        return new $classEntity($result);
    }

    /**
     * @return int
     */
    protected abstract function useType(): int;
}
