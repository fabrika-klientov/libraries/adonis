<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Adonis
 * @category  Services
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Adonis\Services;

use Adonis\Core\Http\HttpClient;
use Adonis\Entities\Paginator as Entities;
use Adonis\Entities\Paginator\Query;
use Adonis\Entities\Shipments\Shipment;
use Illuminate\Support\Collection;

class PaginatorService extends BaseService
{
    protected const COUNT_SHIPMENTS_GROUP = 'shipment-groups/%s/shipments-count';
    protected const SHIPMENTS_SHIPMENTS_GROUP = 'shipment-groups/%s/shipments';
    protected const SHIPMENTS_SHIPMENTS_GROUP_BARCODE = 'shipment-groups/shipments';

    protected $useUserToken = true;

    public function countShipmentsInGroup(string $groupUuid, string $status = null): ?Entities\CountMessage
    {
        return self::factoryEntity(
            self::doRequest(
                HttpClient::GET,
                sprintf(self::COUNT_SHIPMENTS_GROUP, $groupUuid),
                empty($status) ? null : ['status' => $status]
            ),
            Entities\CountMessage::class
        );
    }

    public function getShipmentsByGroup(string $groupUuid, Query\PageQuery $request): Collection
    {
        return self::factoryEntitiesCollect(
            self::doRequest(HttpClient::GET, sprintf(self::SHIPMENTS_SHIPMENTS_GROUP, $groupUuid), $request),
            Shipment::class
        );
    }

    public function getShipmentsByBarcode($barcode, Query\PageQuery $request): Collection
    {
        return self::factoryEntitiesCollect(
            self::doRequest(HttpClient::GET, self::SHIPMENTS_SHIPMENTS_GROUP_BARCODE . "/$barcode", $request),
            Shipment::class
        );
    }

    protected function useType(): int
    {
        return HttpClient::USE_API;
    }
}
