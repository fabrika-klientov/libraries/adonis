<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Adonis
 * @category  Services
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Adonis\Services;

use Adonis\Core\Http\HttpClient;
use Adonis\Entities\Forms as Entities;
use Adonis\Entities\Forms\Query;

class FormsService extends BaseService
{
    protected const SHIPMENT_STICKER = 'shipments/%s/sticker';
    protected const SHIPMENT_STICKER_PARCEL = 'shipments/sticker/parcel';
    protected const SHIPMENT_GROUPS_STICKER = 'shipment-groups/%s/form103a';
    protected const SHIPMENT_FORM119_BY_SHIPMENT = 'shipments/%s/form119';
    protected const SHIPMENT_FORM119_BY_DN = 'shipments/delivery-notifications/%s/form119';
    protected const SHIPMENT_FORM119_BY_SG = 'shipment-groups/%s/form119';
    protected const DOCUMENT_BACK_STICKER = 'document-back/%s/sticker';
    protected const DOCUMENT_BACK_STICKER_BY_SHIPMENT = 'document-back/by/shipment/%s/sticker';
    protected const DOCUMENT_BACK_STICKER_BY_SG = 'shipment-groups/%s/document-back/sticker';
    protected const SHIPMENT_WITH_DOCUMENT_BACK_STICKER_BY_SHIPMENT = 'shipments/%s/with-document-back/sticker';
    protected const SHIPMENT_WITH_DOCUMENT_BACK_STICKER_BY_DB = 'document-back/%s/with-shipment/sticker';
    protected const SHIPMENT_WITH_DOCUMENT_BACK_STICKER_BY_SG = 'shipment-groups/%s/shipment-with-document-back/sticker';
    protected const SHIPMENT_FORM107 = 'shipments/%s/form107';
    protected const SHIPMENT_GROUPS_FORM107 = 'shipment-groups/%s/form107';

    protected $useUserToken = true;

    public function getShipmentSticker($uuidOrBarcode, Query\FormsQuery $query = null): string
    {
        return self::doFormRequest(sprintf(self::SHIPMENT_STICKER, $uuidOrBarcode), $query);
    }

    public function getShipmentParcelSticker($parcelBarcode, Query\FormsQuery $query = null): string
    {
        return self::doFormRequest(self::SHIPMENT_STICKER_PARCEL . "/$parcelBarcode", $query);
    }

    public function getShipmentGroupSticker($groupUuid, Query\FormsQuery $query = null): string
    {
        return self::doFormRequest(sprintf(self::SHIPMENT_GROUPS_STICKER, $groupUuid), $query);
    }

    /**
     * alias for self:getShipmentGroupSticker
     * */
    public function getForm103a($groupUuid, Query\FormsQuery $query = null): string
    {
        return self::getShipmentGroupSticker($groupUuid, $query);
    }

    public function getForm119ByShipment($uuidOrBarcode, Query\FormsQuery $query = null): string
    {
        return self::doFormRequest(sprintf(self::SHIPMENT_FORM119_BY_SHIPMENT, $uuidOrBarcode), $query);
    }

    public function getForm119ByDeliveryNotification($uuidOrBarcode, Query\FormsQuery $query = null): string
    {
        return self::doFormRequest(sprintf(self::SHIPMENT_FORM119_BY_DN, $uuidOrBarcode), $query);
    }

    public function getForm119ByShipmentGroup($uuid, Query\FormsQuery $query = null): string
    {
        return self::doFormRequest(sprintf(self::SHIPMENT_FORM119_BY_SG, $uuid), $query);
    }

    public function getDocumentBackSticker($uuid, Query\FormsQuery $query = null): string
    {
        return self::doFormRequest(sprintf(self::DOCUMENT_BACK_STICKER, $uuid), $query);
    }

    public function getDocumentBackStickerByShipment($uuid, Query\FormsQuery $query = null): string
    {
        return self::doFormRequest(sprintf(self::DOCUMENT_BACK_STICKER_BY_SHIPMENT, $uuid), $query);
    }

    public function getDocumentBackStickerByShipmentGroup($uuid, Query\FormsQuery $query = null): string
    {
        return self::doFormRequest(sprintf(self::DOCUMENT_BACK_STICKER_BY_SG, $uuid), $query);
    }

    public function getShipmentWithDocumentBackStickerByShipment($uuidOrBarcode, Query\FormsQuery $query = null): string
    {
        return self::doFormRequest(
            sprintf(self::SHIPMENT_WITH_DOCUMENT_BACK_STICKER_BY_SHIPMENT, $uuidOrBarcode),
            $query
        );
    }

    public function getShipmentWithDocumentBackStickerByDocumentBack(
        $uuidOrBarcode,
        Query\FormsQuery $query = null
    ): string {
        return self::doFormRequest(sprintf(self::SHIPMENT_WITH_DOCUMENT_BACK_STICKER_BY_DB, $uuidOrBarcode), $query);
    }

    public function getShipmentWithDocumentBackStickerByShipmentGroup($uuid, Query\FormsQuery $query = null): string
    {
        return self::doFormRequest(sprintf(self::SHIPMENT_WITH_DOCUMENT_BACK_STICKER_BY_SG, $uuid), $query);
    }

    public function getShipmentForm107ByShipment($uuidOrBarcode, Query\FormsQuery $query = null): string
    {
        return self::doFormRequest(sprintf(self::SHIPMENT_FORM107, $uuidOrBarcode), $query);
    }

    public function getShipmentForm107ByShipmentGroup($uuid, Query\FormsQuery $query = null): string
    {
        return self::doFormRequest(sprintf(self::SHIPMENT_GROUPS_FORM107, $uuid), $query);
    }

    protected function doFormRequest(string $link, ?Query\FormsQuery $query)
    {
        return $this
            ->getHttpClient()
            ->request(HttpClient::GET, $link, ['query' => isset($query) ? $query->getData() : []], true);
    }

    protected function useType(): int
    {
        return HttpClient::USE_FORM;
    }
}
