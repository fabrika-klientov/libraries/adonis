<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Adonis
 * @category  Services
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Adonis\Services;

use Adonis\Core\Http\HttpClient;
use Adonis\Entities\Clients as Entities;
use Adonis\Entities\Clients\Requests;
use Illuminate\Support\Collection;

class ClientsService extends BaseService
{
    protected const CLIENTS = 'clients';
    protected const CLIENTS_BY_EXTERNAL = 'clients/external-id';
    protected const CLIENTS_BY_PHONE = 'clients/phone';
    protected const CLIENTS_ADDRESSES = 'client-addresses';
    protected const CLIENTS_EMAILS = 'client-emails';
    protected const CLIENTS_PHONES = 'client-phones';
    protected const PHONES_PROHIBITED = 'phones/UA/prohibited';
    protected const PHONES = 'phones';
    protected const CLIENTS_POST_PAY_RECIPIENTS = 'clients/%s/post-pay-recipients';

    protected $useUserToken = true;

    // storing clients

    public function store(Requests\StoreClientRequest $request): ?Entities\Client
    {
        return self::factoryEntity(
            self::doRequest(HttpClient::POST, self::CLIENTS, $request),
            Entities\Client::class
        );
    }

    public function storeList(Collection $list): Collection
    {
        return self::factoryEntitiesCollect(
            self::doRequest(HttpClient::POST, self::CLIENTS, $list),
            Entities\Client::class
        );
    }

    // getting clients

    public function findByUuid($uuid): ?Entities\Client
    {
        return self::factoryEntity(
            self::doRequest(HttpClient::GET, self::CLIENTS . "/$uuid", null),
            Entities\Client::class
        );
    }

    public function findByExternalId($externalId): ?Entities\Client
    {
        return self::factoryEntity(
            self::doRequest(HttpClient::GET, self::CLIENTS_BY_EXTERNAL . "/$externalId", null),
            Entities\Client::class
        );
    }

    public function getClientSelectively($uuid, array $keys): ?Entities\Client
    {
        return self::factoryEntity(
            self::doRequest(HttpClient::POST, self::CLIENTS . "/$uuid", $keys),
            Entities\Client::class
        );
    }

    public function findByPhoneNumber($phone, string $countryISO3166 = 'UA'): ?Entities\Client
    {
        return self::factoryEntity(
            self::doRequest(
                HttpClient::GET,
                self::CLIENTS_BY_PHONE,
                ['phoneNumber' => $phone, 'countryISO3166' => $countryISO3166]
            ),
            Entities\Client::class
        );
    }

    // updating clients

    public function update($uuid, Requests\UpdateClientRequest $request): ?Entities\Client
    {
        return self::factoryEntity(
            self::doRequest(HttpClient::PUT, self::CLIENTS . "/$uuid", $request),
            Entities\Client::class
        );
    }

    // linked Client data (addresses, phones, emails)

    public function clientAddresses($clientUuid): Collection
    {
        return self::factoryEntitiesCollect(
            self::doRequest(HttpClient::GET, self::CLIENTS_ADDRESSES, ['clientUuid' => $clientUuid]),
            Entities\ClientAddress::class
        );
    }

    public function clientEmails($clientUuid): Collection
    {
        return self::factoryEntitiesCollect(
            self::doRequest(HttpClient::GET, self::CLIENTS_EMAILS, ['clientUuid' => $clientUuid]),
            Entities\ClientEmail::class
        );
    }

    public function clientPhones($clientUuid): Collection
    {
        return self::factoryEntitiesCollect(
            self::doRequest(HttpClient::GET, self::CLIENTS_PHONES, ['clientUuid' => $clientUuid]),
            Entities\ClientPhone::class
        );
    }

    public function deletePhone($phoneUuid): void
    {
        self::doRequest(HttpClient::DELETE, self::CLIENTS_PHONES . "/$phoneUuid", null);
    }

    public function deleteAddress($addressUuid): void
    {
        self::doRequest(HttpClient::DELETE, self::CLIENTS_ADDRESSES . "/$addressUuid", null);
    }

    public function deleteEmail($emailUuid): void
    {
        self::doRequest(HttpClient::DELETE, self::CLIENTS_EMAILS . "/$emailUuid", null);
    }

    public function phonesProhibited(): Collection
    {
        return self::factoryEntitiesCollect(
            self::doRequest(HttpClient::GET, self::PHONES_PROHIBITED, null),
            Entities\PhoneProhibited::class
        );
    }

    // phone prohibited

    public function checkPhoneProhibited(string $phone): ?Entities\PhoneProhibited
    {
        return self::factoryEntity(
            self::doRequest(HttpClient::GET, self::PHONES_PROHIBITED . "/$phone", null),
            Entities\PhoneProhibited::class
        );
    }

    public function findPhoneById($id): ?Entities\Phone
    {
        return self::factoryEntity(
            self::doRequest(HttpClient::GET, self::PHONES . "/$id", null),
            Entities\Phone::class
        );
    }

    // post pay recipients

    public function addPostPayRecipient(string $clientUuid, string $postPayRecipientUuid): ?Entities\Message
    {
        return self::factoryEntity(
            self::doRequest(
                HttpClient::POST,
                sprintf(self::CLIENTS_POST_PAY_RECIPIENTS, $clientUuid) . "/$postPayRecipientUuid",
                null
            ),
            Entities\Message::class
        );
    }

    public function clientPostPayRecipients(string $clientUuid): Collection
    {
        return self::factoryEntitiesCollect(
            self::doRequest(HttpClient::GET, sprintf(self::CLIENTS_POST_PAY_RECIPIENTS, $clientUuid), null),
            Entities\ClientPostPayRecipient::class
        );
    }

    public function deletePostPayRecipients(string $clientUuid, string $postPayRecipientUuid): ?Entities\Message
    {
        return self::factoryEntity(
            self::doRequest(
                HttpClient::DELETE,
                sprintf(self::CLIENTS_POST_PAY_RECIPIENTS, $clientUuid) . "/$postPayRecipientUuid",
                null
            ),
            Entities\Message::class
        );
    }

    protected function useType(): int
    {
        return HttpClient::USE_API;
    }
}
