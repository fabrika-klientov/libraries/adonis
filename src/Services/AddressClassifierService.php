<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Adonis
 * @category  Services
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Adonis\Services;

use Adonis\Core\Http\HttpClient;
use Adonis\Entities\AddressClassifier as Entities;
use Adonis\Entities\AddressClassifier\Query;
use Illuminate\Support\Collection;

class AddressClassifierService extends BaseService
{
    protected const GET_REGIONS = 'get_regions_by_region_ua';
    protected const GET_DISTRICTS = 'get_districts_by_region_id_and_district_ua';
    protected const GET_CITIES = 'get_city_by_region_id_and_district_id_and_city_ua';
    protected const GET_STREETS = 'get_street_by_region_id_and_district_id_and_city_id_and_street_ua';
    protected const GET_ADDRESSES = 'get_addr_house_by_street_id';
    protected const GET_COURIER_AREA = 'get_courierarea_by_postindex';
    protected const GET_POST_OFFICES = 'get_postoffices_by_postindex';
    protected const GET_POST_OFFICES_OPEN_HOURS = 'get_postoffices_openhours_by_postindex';
    protected const GET_POST_OFFICES_MOBILE_OPEN_HOURS = 'get_postoffices_mobile_openhours_by_postindex';
    protected const GET_POST_OFFICES_GEO = 'get_postoffices_by_geolocation';
    protected const GET_POST_OFFICES_CITY = 'get_postoffices_by_city_id';
    protected const GET_POST_OFFICES_POSTCODE = 'get_postoffices_by_postcode_cityid_cityvpzid';
    protected const GET_CITY_DETAILS = 'get_city_details_by_postcode';
    protected const GET_ADDRESS_DETAILS = 'get_address_by_postcode';
    protected const GET_POSTCODE = 'get_postcode_by_city_id';
    protected const GET_DISTRICTS_NAME = 'get_district_by_name';
    protected const GET_CITIES_NAME = 'get_city_by_name';
    protected const GET_STREETS_NAME = 'get_street_by_name';

    protected $keysCollect = ['Entries', 'Entry'];

    public function getRegions(Query\RegionsQuery $query = null): Collection
    {
        return self::factoryEntitiesCollect(
            self::doRequest(HttpClient::GET, self::GET_REGIONS, $query),
            Entities\Region::class
        );
    }

    public function getDistricts(Query\DistrictsQuery $query): Collection
    {
        return self::factoryEntitiesCollect(
            self::doRequest(HttpClient::GET, self::GET_DISTRICTS, $query),
            Entities\District::class
        );
    }

    public function getCities(Query\CitiesQuery $query): Collection
    {
        return self::factoryEntitiesCollect(
            self::doRequest(HttpClient::GET, self::GET_CITIES, $query),
            Entities\City::class
        );
    }

    public function getStreets(Query\StreetsQuery $query): Collection
    {
        return self::factoryEntitiesCollect(
            self::doRequest(HttpClient::GET, self::GET_STREETS, $query),
            Entities\Street::class
        );
    }

    public function getAddresses(Query\AddressesQuery $query): Collection
    {
        return self::factoryEntitiesCollect(
            self::doRequest(HttpClient::GET, self::GET_ADDRESSES, $query),
            Entities\Address::class
        );
    }

    public function getCourierArea(Query\CourierAreaQuery $query): Collection
    {
        return self::factoryEntitiesCollect(
            self::doRequest(HttpClient::GET, self::GET_COURIER_AREA, $query),
            Entities\CourierArea::class
        );
    }

    public function getPostOffices(Query\PostOfficesQuery $query): Collection
    {
        return self::factoryEntitiesCollect(
            self::doRequest(HttpClient::GET, self::GET_POST_OFFICES, $query),
            Entities\PostOffice::class
        );
    }

    public function getPostOfficesOpenHours(Query\PostOfficesOpenHoursQuery $query): Collection
    {
        return self::factoryEntitiesCollect(
            self::doRequest(HttpClient::GET, self::GET_POST_OFFICES_OPEN_HOURS, $query),
            Entities\PostOfficeOpenHours::class
        );
    }

    public function getPostOfficesMobileOpenHours(Query\PostOfficesMobileOpenHoursQuery $query): Collection
    {
        return self::factoryEntitiesCollect(
            self::doRequest(HttpClient::GET, self::GET_POST_OFFICES_MOBILE_OPEN_HOURS, $query),
            Entities\PostOfficeMobileOpenHours::class
        );
    }

    public function getPostOfficesByGeo(Query\PostOfficesByGeoQuery $query): Collection
    {
        return self::factoryEntitiesCollect(
            self::doRequest(HttpClient::GET, self::GET_POST_OFFICES_GEO, $query),
            Entities\PostOfficeGeo::class);
    }

    public function getPostOfficesByCity(Query\PostOfficesByCityQuery $query): Collection
    {
        return self::factoryEntitiesCollect(
            self::doRequest(HttpClient::GET, self::GET_POST_OFFICES_CITY, $query),
            Entities\PostOfficeCity::class
        );
    }

    public function getCityDetails(Query\CityDetailsQuery $query): Collection
    {
        return self::factoryEntitiesCollect(
            self::doRequest(HttpClient::GET, self::GET_CITY_DETAILS, $query),
            Entities\CityDetails::class
        );
    }

    public function getAddressDetails(Query\AddressDetailsQuery $query): Collection
    {
        return self::factoryEntitiesCollect(
            self::doRequest(HttpClient::GET, self::GET_ADDRESS_DETAILS, $query),
            Entities\AddressDetails::class
        );
    }

    public function getPostCode(Query\PostCodeQuery $query): Collection
    {
        return self::factoryEntitiesCollect(
            self::doRequest(HttpClient::GET, self::GET_POSTCODE, $query),
            Entities\PostCode::class
        );
    }

    public function getPostOfficesPostcode(Query\PostOfficesPostCodeQuery $query): Collection
    {
        return self::factoryEntitiesCollect(
            self::doRequest(HttpClient::GET, self::GET_POST_OFFICES_POSTCODE, $query),
            Entities\PostOfficePostCode::class
        );
    }

    public function getDistrictsName(Query\DistrictsNameQuery $query): Collection
    {
        return self::factoryEntitiesCollect(
            self::doRequest(HttpClient::GET, self::GET_DISTRICTS_NAME, $query),
            Entities\DistrictName::class
        );
    }

    public function getCitiesName(Query\CitiesNameQuery $query): Collection
    {
        return self::factoryEntitiesCollect(
            self::doRequest(HttpClient::GET, self::GET_CITIES_NAME, $query),
            Entities\CityName::class);
    }

    public function getStreetsName(Query\StreetsNameQuery $query): Collection
    {
        return self::factoryEntitiesCollect(
            self::doRequest(HttpClient::GET, self::GET_STREETS_NAME, $query),
            Entities\StreetName::class
        );
    }

    protected function useType(): int
    {
        return HttpClient::USE_ADDRESS_CLASSIFIER;
    }
}
