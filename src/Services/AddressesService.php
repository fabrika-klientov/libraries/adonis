<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Adonis
 * @category  Services
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Adonis\Services;

use Adonis\Core\Http\HttpClient;
use Adonis\Entities\Addresses as Entities;
use Adonis\Entities\Addresses\Requests;

class AddressesService extends BaseService
{
    protected const ADDRESSES = 'addresses';

    public function store(Requests\StoreAddressRequest $request): ?Entities\Address
    {
        return self::factoryEntity(
            self::doRequest(HttpClient::POST, self::ADDRESSES, $request),
            Entities\Address::class
        );
    }

    public function findById($id): ?Entities\Address
    {
        return self::factoryEntity(
            self::doRequest(HttpClient::GET, self::ADDRESSES . "/$id", null),
            Entities\Address::class
        );
    }

    public function update($id, Requests\UpdateAddressRequest $request): ?Entities\Address
    {
        return self::factoryEntity(
            self::doRequest(HttpClient::PUT, self::ADDRESSES . "/description/$id", $request),
            Entities\Address::class
        );
    }

    protected function useType(): int
    {
        return HttpClient::USE_API;
    }
}
