<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Adonis
 * @category  Services
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Adonis\Services;

use Adonis\Core\Http\HttpClient;
use Adonis\Entities\Shipments as Entities;
use Adonis\Entities\Shipments\Requests;
use Illuminate\Support\Collection;

class ShipmentsService extends BaseService
{
    protected const SHIPMENTS = 'shipments';
    protected const SHIPMENTS_BARCODE = 'shipments/barcode';
    protected const SHIPMENTS_BARCODE_PRICE_CHANGED_IN_POST_OFFICE = 'shipments/barcode/%s/isPriceChangedInPostOffice';
    protected const SHIPMENTS_LIFECYCLE = 'shipments/%s/lifecycle';
    protected const SHIPMENTS_PRICE_CHANGED_IN_POST_OFFICE = 'shipments/sender/%s/from/%s/to/%s/isPriceChangedInPostOffice';
    protected const SHIPMENTS_PARCELS = 'shipments/%s/parcels';
    protected const SHIPMENTS_FREE = 'shipments/loyalty-program/free';
    protected const COURIER_SERVICE_ORDERS = 'courier-service/orders';
    protected const CLIENTS_COURIER_SERVICE_ORDERS = 'clients/%s/courier-delivery-orders';
    protected const COURIER_SERVICE_ORDERS_STATUSES = 'courier-service/orders/%s/statuses';
    protected const SHIPMENTS_POST_PAY_DIVIDE = 'shipments/%s/postPayDivide';
    protected const SHIPMENTS_POST_PAY_TRANSFERS = 'transfers/shipment-postpays/%s/with-recipient';
    protected const BARCODES = 'barcodes';
    protected const BARCODES_LIST = 'barcodes/all-of';

    protected $useUserToken = true;

    // storing shipments

    public function store(Requests\StoreShipmentRequest $request): ?Entities\Shipment
    {
        return self::factoryEntity(
            self::doRequest(HttpClient::POST, self::SHIPMENTS, $request),
            Entities\Shipment::class
        );
    }

    public function storeList(Collection $list): Collection
    {
        return self::factoryEntitiesCollect(
            self::doRequest(HttpClient::POST, self::SHIPMENTS, $list),
            Entities\Shipment::class
        );
    }

    public function storeByLoyaltyProgram(Requests\StoreShipmentRequest $request): ?Entities\Shipment
    {
        return self::factoryEntity(
            self::doRequest(HttpClient::POST, self::SHIPMENTS_FREE, $request),
            Entities\Shipment::class
        );
    }

    // updating shipments

    public function update(string $uuid, Requests\UpdateShipmentRequest $request): ?Entities\Shipment
    {
        return self::factoryEntity(
            self::doRequest(HttpClient::PUT, self::SHIPMENTS . "/$uuid", $request),
            Entities\Shipment::class
        );
    }

    public function updateList(Collection $list): Collection
    {
        return self::factoryEntitiesCollect(
            self::doRequest(HttpClient::PUT, self::SHIPMENTS, $list),
            Entities\ShipmentUpdatedMessage::class
        );
    }

    public function addParcel(string $uuidOrBarcode, Requests\StoreShipmentParcelRequest $request): ?Entities\Shipment
    {
        return self::factoryEntity(
            self::doRequest(HttpClient::POST, sprintf(self::SHIPMENTS_PARCELS, $uuidOrBarcode), $request),
            Entities\Shipment::class
        );
    }

    public function deleteParcel(string $uuid, $parcelUuidOrBarcode): ?Entities\Shipment
    {
        return self::factoryEntity(
            self::doRequest(
                HttpClient::DELETE,
                sprintf(self::SHIPMENTS_PARCELS, $uuid) . "/$parcelUuidOrBarcode",
                null
            ),
            Entities\Shipment::class
        );
    }

    // get shipment

    public function findByUuid($uuid): ?Entities\Shipment
    {
        return self::factoryEntity(
            self::doRequest(HttpClient::GET, self::SHIPMENTS . "/$uuid", null),
            Entities\Shipment::class
        );
    }

    public function findByBarCode($barcode): ?Entities\Shipment
    {
        return self::factoryEntity(
            self::doRequest(HttpClient::GET, self::SHIPMENTS_BARCODE . "/$barcode", null),
            Entities\Shipment::class
        );
    }

    public function getShipmentSelectively($shipmentUuidOrBarcode, array $keys): ?Entities\Shipment
    {
        return self::factoryEntity(
            self::doRequest(HttpClient::POST, self::SHIPMENTS . "/$shipmentUuidOrBarcode", $keys),
            Entities\Shipment::class
        );
    }

    // delete shipment

    public function deleteShipment($uuid): void
    {
        self::doRequest(HttpClient::DELETE, self::SHIPMENTS . "/$uuid", null);
    }

    // statuses

    public function getPriceChanged($shipmentBarcode): ?Entities\PriceChangedMessage
    {
        return self::factoryEntity(
            self::doRequest(
                HttpClient::GET,
                sprintf(self::SHIPMENTS_BARCODE_PRICE_CHANGED_IN_POST_OFFICE, $shipmentBarcode),
                null
            ),
            Entities\PriceChangedMessage::class
        );
    }

    public function getPriceChangedList(
        string $senderUuid,
        string $fromDate,
        string $toDate,
        bool $barcodes = true
    ): ?array {
        return self::doRequest(
            HttpClient::GET,
            sprintf(self::SHIPMENTS_PRICE_CHANGED_IN_POST_OFFICE, $senderUuid, $fromDate, $toDate),
            ['barcodes' => $barcodes ? 'true' : 'false']
        );
    }

    public function getLifecycle($barcodeOrUuid): ?Entities\ShipmentLifecycle
    {
        return self::factoryEntity(
            self::doRequest(HttpClient::GET, sprintf(self::SHIPMENTS_LIFECYCLE, $barcodeOrUuid), null),
            Entities\ShipmentLifecycle::class
        );
    }

    // courier service orders

    public function storeCourierServiceOrders(
        Requests\StoreCourierServiceOrdersRequest $request
    ): ?Entities\CourierServiceOrder {
        return self::factoryEntity(
            self::doRequest(HttpClient::POST, self::COURIER_SERVICE_ORDERS, $request),
            Entities\CourierServiceOrder::class
        );
    }

    public function courierServiceOrdersList(string $clientUuid): Collection
    {
        return self::factoryEntitiesCollect(
            self::doRequest(HttpClient::GET, sprintf(self::CLIENTS_COURIER_SERVICE_ORDERS, $clientUuid), null),
            Entities\CourierServiceOrder::class
        );
    }

    public function findCourierServiceOrders(string $orderUuid): ?Entities\CourierServiceOrder
    {
        return self::factoryEntity(
            self::doRequest(HttpClient::GET, self::COURIER_SERVICE_ORDERS . "/$orderUuid", null),
            Entities\CourierServiceOrder::class
        );
    }

    public function getStatusesCourierServiceOrders(string $orderUuid): Collection
    {
        return self::factoryEntitiesCollect(
            self::doRequest(HttpClient::GET, sprintf(self::COURIER_SERVICE_ORDERS_STATUSES, $orderUuid), null),
            Entities\CourierServiceOrderStatus::class
        );
    }

    // post pay divide (slices)

    public function getShipmentPostPayDivide($uuidOrBarcode): Collection
    {
        return self::factoryEntitiesCollect(
            self::doRequest(HttpClient::GET, sprintf(self::SHIPMENTS_POST_PAY_DIVIDE, $uuidOrBarcode), null),
            Entities\ShipmentPostPaySlice::class
        );
    }

    public function getStatusShipmentPostPay($barcode): ?Entities\ShipmentPostPayStatus
    {
        return self::factoryEntity(
            self::doRequest(HttpClient::GET, sprintf(self::SHIPMENTS_POST_PAY_TRANSFERS, $barcode), null),
            Entities\ShipmentPostPayStatus::class
        );
    }

    // barcodes

    public function getTypeBarcode($barcode): ?Entities\BarcodeType
    {
        return self::factoryEntity(
            self::doRequest(HttpClient::GET, self::BARCODES . "/$barcode", null),
            Entities\BarcodeType::class
        );
    }

    public function getTypeBarcodes(array $barcodes): Collection
    {
        if (empty($barcodes)) {
            return new Collection();
        }

        $barcodes = join('&', $barcodes); // TODO: multiple doesn't work
        return self::factoryEntitiesCollect(
            self::doRequest(HttpClient::GET, self::BARCODES_LIST . "/$barcodes", null),
            Entities\BarcodeType::class
        );
    }

    protected function useType(): int
    {
        return HttpClient::USE_API;
    }
}
